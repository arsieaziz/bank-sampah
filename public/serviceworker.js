var staticCacheName = "pwa-v" + new Date().getTime();
var filesToCache = [
    '/offline',
    '/css/mudahsaja/vendor.min.css',
    '/css/mudahsaja/style1.min.css',
    '/css/mudahsaja/style2.min.css',
    '/js/mudahsaja/vendor.min.js',
    '/js/mudahsaja/main.min.js',
    '/images/logo.png',
    '/images/favicon/icon-16x16.png',
    '/images/favicon/icon-32x32.png',
    '/images/favicon/icon-36x36.png',
    '/images/favicon/icon-48x48.png',
    '/images/favicon/icon-72x72.png',
    '/images/favicon/icon-96x96.png',
    '/images/favicon/icon-120x120.png',
    '/images/favicon/icon-144x144.png',
    '/images/favicon/icon-152x152.png',
    '/images/favicon/icon-192x192.png',
    '/images/favicon/icon-310x310.png',
    '/images/favicon/icon-512x512.png',
    '/images/favicon/mask-196x196.png',
    '/images/favicon/splash-240x320.png',
    '/images/favicon/splash-320x480.png',
    '/images/favicon/splash-480x800.png',
    '/images/favicon/splash-720x1280.png',
    '/images/favicon/splash-960x1600.png',
    '/images/favicon/splash-1280x1920.png',
    '/fonts/webfonts/fa-brands-400.ttf',
    '/fonts/webfonts/fa-brands-400.woff2',
    '/fonts/webfonts/fa-regular-400.ttf',
    '/fonts/webfonts/fa-regular-400.woff2',
    '/fonts/webfonts/fa-solid-900.ttf',
    '/fonts/webfonts/fa-solid-900.woff2',
    '/fonts/webfonts/fa-v4compatibility.ttf',
    '/fonts/webfonts/fa-v4compatibility.woff2'
];

// Cache on install
self.addEventListener("install", event => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    )
});

// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                return caches.match('offline');
            })
    )
});