-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Nov 2023 pada 12.21
-- Versi server: 8.0.31
-- Versi PHP: 8.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banksampah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `members`
--

CREATE TABLE `members` (
  `id` bigint UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_birth` date NOT NULL,
  `number_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` date DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `members`
--

INSERT INTO `members` (`id`, `fullname`, `date_birth`, `number_phone`, `email`, `password`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Muhaimin Iskandar', '2019-11-01', '081957381623', NULL, '$2y$10$x/9AbtOdxUT/rrbh8HZbFO5tUJKr3SpmGQExRxpGuVsW/D7OuTSSi', 1, '2023-11-14 10:07:13', '2023-11-14 10:09:19');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_12_02_062729_create_settings_table', 1),
(6, '2022_12_04_054646_create_roles_table', 1),
(7, '2023_05_09_123342_create_produks_table', 1),
(8, '2023_05_09_135634_create_photos_table', 1),
(9, '2023_05_09_230203_create_tentangs_table', 1),
(10, '2023_05_15_134838_create_members_table', 1),
(11, '2023_05_26_114630_create_sales_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `photos`
--

CREATE TABLE `photos` (
  `id` bigint UNSIGNED NOT NULL,
  `parent_id` bigint UNSIGNED DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `photos`
--

INSERT INTO `photos` (`id`, `parent_id`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'c6a1a710b676dd2b0bcf31d3b274cd90-16999452618889.jpeg', '2023-11-14 07:01:01', '2023-11-14 07:01:01'),
(2, 1, 'istockphoto-473676558-612x612-16999452617099.jpg', '2023-11-14 07:01:01', '2023-11-14 07:01:01'),
(3, 2, '20200929-bestua-16999574247134.jpg', '2023-11-14 10:23:44', '2023-11-14 10:23:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produks`
--

CREATE TABLE `produks` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booked` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `sale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `active` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `produks`
--

INSERT INTO `produks` (`id`, `name`, `slug`, `price`, `description`, `main_image`, `booked`, `sale`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Kardus', 'kardus', 25000.00, '<p>Karton atau kardus adalah bahan yang terbuat dari pulp, baik itu pulp organik, pulp sintetis atau sisa produksi kertas. Merujuk pada ISO 536, sebuah bahan yang terbuat dari kertas dengan berat lebih dari 200 g/m2 dikategorikan sebagai karton.<br></p>', 'kardus-packing-box-kardus-tambahan-packing-16999452315818.jpg', '0', '2.4', 1, '2023-11-14 07:00:31', '2023-11-14 11:13:21'),
(2, 'Besi', 'besi', 80000.00, '<p>Besi adalah unsur kimia dengan simbol Fe dan nomor atom 26. Besi merupakan logam dalam deret transisi pertama. Besi adalah unsur paling umum di bumi berdasarkan massa, membentuk sebagian besar bagian inti luar dan dalam bumi. Besi adalah unsur keempat terbesar pada kerak bumi.<br></p>', '103fmtt-16999574125574.jpg', '0', '0.4', 1, '2023-11-14 10:23:32', '2023-11-14 11:13:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `access`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '[\"users\",\"roles\",\"members\",\"settings\",\"tentang\",\"produk\",\"penjualan\"]', '2023-11-14 12:13:22', '2023-11-14 12:13:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sales`
--

CREATE TABLE `sales` (
  `id` bigint UNSIGNED NOT NULL,
  `member_id` bigint UNSIGNED NOT NULL,
  `product` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sales`
--

INSERT INTO `sales` (`id`, `member_id`, `product`, `jumlah_produk`, `total_harga`, `catatan`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '[{\"id\":2,\"title\":\"Besi\",\"url\":\"katalog/besi\",\"img\":\"storage/images/thumbnail/103fmtt-16999574125574.jpg\",\"price\":80000,\"pay\":32000,\"qty\":\"0.4\"},{\"id\":1,\"title\":\"Kardus\",\"url\":\"katalog/kardus\",\"img\":\"storage/images/thumbnail/kardus-packing-box-kardus-tambahan-packing-16999452315818.jpg\",\"price\":25000,\"pay\":60000,\"qty\":\"2.4\"}]', '2', '92000', 'Cairin Cepet yak', 'done', '2023-11-14 10:42:28', '2023-11-14 11:13:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `name`, `description`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'web_title', 'Web Title', 'text', 'Bank Sampah', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(2, 'web_description', 'Web Description', 'textarea', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe iste laboriosam asperiores ipsam recusandae nulla beatae laborum voluptatem eum hic ipsum animi quae veritatis tempora aut, ad, quia harum qui!', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(3, 'web_keyword', 'Web Keyword', 'text', 'Website, Apps, Bank Sampah', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(4, 'address', 'Address', 'textarea', 'Kel Talang, Kec. Telukbetung Selatan, Kota Bandar Lampung, Lampung', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(5, 'telp', 'Telepon', 'text', '(0721) 781740', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(6, 'email', 'Email', 'text', 'admin@gmail.com', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(7, 'instagram', 'Instagram', 'text', 'arsieaziz', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(8, 'latlong', 'Latitude & Longitude', 'text', '-5.3573825,105.2668444', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(9, 'Senin-Sabtu', '08:00 - 17:00', 'open_time', '', '2023-11-14 06:44:21', '2023-11-14 06:44:21'),
(10, 'dana', 'E-Wallet', 'payment_method', '081957381623', '2023-11-14 06:45:01', '2023-11-14 06:45:01'),
(11, 'image', 'Image', 'logo', 'trash-bin-16999459346491.png', '2023-05-06 08:44:00', '2023-11-14 07:12:14'),
(12, 'text_first', '#619cd6', 'logo', 'Bank', '2023-05-07 04:13:43', '2023-11-14 07:31:37'),
(13, 'text_second', '#619cd6', 'logo', 'Sampah', NULL, '2023-11-14 07:31:37'),
(14, 'image', 'Image', 'hero', 'hero-16840775441961.jpg', '2023-05-06 08:44:00', '2023-05-14 15:19:05'),
(15, 'title', 'Title', 'hero', 'Selamat Datang di Bank Sampah', '2023-05-07 04:13:43', '2023-11-14 07:23:00'),
(16, 'description', 'descript', 'hero', 'Sekarang semua bisa ubah sampah menjadi uang!', '2023-05-07 06:53:30', '2023-11-14 07:23:00'),
(17, 'mappoint', '-', 'pinpoint', '-5.444440,105.276146', '2023-05-07 11:28:10', '2023-11-14 07:23:14'),
(18, 'facebook', 'Facebook', 'socmed', 'arsie.aziz', '2023-05-07 14:26:49', '2023-11-14 07:24:08'),
(19, 'whatsapp', 'Whatsapp', 'socmed', '081957381623', '2023-05-07 14:26:49', '2023-11-14 07:24:08'),
(20, 'instagram', 'Instagram', 'socmed', 'arsieaziz', '2023-05-07 14:26:49', '2023-11-14 07:24:08'),
(21, 'twitter', 'Twitter', 'socmed', 'arsieaziz', '2023-05-07 14:26:49', '2023-11-14 07:24:08'),
(22, 'youtube', 'Youtube', 'socmed', '-', '2023-05-07 14:26:49', '2023-11-14 07:24:08'),
(23, 'tiktok', 'TikTok', 'socmed', '-', '2023-05-07 14:26:49', '2023-11-14 07:24:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentangs`
--

CREATE TABLE `tentangs` (
  `id` bigint UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `whatsapp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tentangs`
--

INSERT INTO `tentangs` (`id`, `value`, `view`, `facebook`, `twitter`, `whatsapp`, `created_at`, `updated_at`) VALUES
(1, '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vel malesuada justo, nec euismod leo. Donec facilisis metus in sem convallis blandit. Donec laoreet a dui id elementum. Donec vestibulum et mauris quis vehicula. Cras posuere augue sed augue molestie dapibus. Donec lacinia neque nec neque facilisis ullamcorper vel sed risus. Aenean ultricies maximus tortor, id varius erat maximus id. Nunc euismod pretium turpis. Nullam nec mattis purus, quis sagittis lorem. Nullam blandit enim risus, nec dictum justo feugiat nec. Nam orci nisi, volutpat sodales justo dictum, viverra molestie augue. Aenean sit amet leo hendrerit, mollis nisl vitae, ullamcorper sapien.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Sed viverra faucibus lorem, ac viverra elit euismod id. In sed leo dapibus mi interdum vehicula. Aenean pharetra facilisis nisi, suscipit tempus libero molestie ac. Proin sollicitudin id neque eget tincidunt. Curabitur eu auctor metus. Suspendisse id felis ac leo rhoncus placerat quis at libero. Nulla ut lectus tempor, egestas lectus vitae, rhoncus nisi. Nullam et venenatis turpis, a sodales orci. Suspendisse pharetra leo porta, venenatis tellus eget, malesuada felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus at tincidunt risus, at ultricies lorem. Fusce ac fringilla erat. Vestibulum quis urna vel nisl dignissim blandit dapibus quis quam. Ut ipsum turpis, sagittis et diam ac, consectetur placerat nunc.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">Ut hendrerit aliquam lacus, ut convallis ante elementum quis. Nam sit amet vestibulum erat, non hendrerit ligula. Integer mi turpis, gravida vitae libero sit amet, suscipit dapibus elit. Donec scelerisque efficitur varius. Quisque quis bibendum lorem. Phasellus elementum mi urna. Morbi leo tortor, mollis sed libero eget, congue mollis leo. Sed ultricies nulla mi, sed varius nulla sagittis vel. Sed pharetra arcu rutrum vulputate vulputate. Cras gravida lacus vel scelerisque sagittis. Vivamus orci metus, suscipit nec ex sit amet, dictum rhoncus augue. Sed at massa vitae lectus varius vestibulum at ut elit. Phasellus lobortis, odio nec tristique pharetra, magna libero feugiat odio, eu finibus nunc magna nec sem.</p>', '5', '0', '0', '0', NULL, '2023-11-14 12:19:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `position`, `username`, `email`, `role_id`, `image`, `email_verified_at`, `password`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Quinn Brown MD', 'Direktur', 'admin', 'hand.kiel@example.net', 0, NULL, '2023-11-14 06:35:02', '$2y$10$6aDssOgnr/KglHpbazH/8.XmxPI/hvTUYpd53SzIWaEwAnBkSZdBq', 1, '4UeGahD5Dhfr6qZCYP9gQcxakGw73m3gEtwSOsL7RNC3k1qv8fSLGJWBOAC0', '2023-11-14 06:35:02', '2023-11-14 06:35:02'),
(2, 'Admin Web', 'Administrators', 'adminweb', 'admin@admin.com', 1, 'bus-16999640715523.png', '2023-11-14 12:14:31', '$2y$10$a6aoBLkkCQNgoRWm1Bt/veoMZ48DwtV7UviIZoE.LMIIGDpcV6X4C', 1, NULL, '2023-11-14 12:14:31', '2023-11-14 12:14:31');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_number_phone_unique` (`number_phone`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `produks`
--
ALTER TABLE `produks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `produks_name_unique` (`name`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indeks untuk tabel `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tentangs`
--
ALTER TABLE `tentangs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `photos`
--
ALTER TABLE `photos`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `produks`
--
ALTER TABLE `produks`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `tentangs`
--
ALTER TABLE `tentangs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
