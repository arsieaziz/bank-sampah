<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\Admin as Admin;
use App\Http\Controllers\KatalogController;
use App\Http\Controllers\KeranjangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('home');

Route::get('offline', [FrontendController::class, 'offline']);

Route::get('tentang', [FrontendController::class, 'tentang']);
Route::put('tentang/view/{id}', [FrontendController::class, 'tentang_count_view']);
Route::put('tentang/facebook/{id}', [FrontendController::class, 'tentang_count_facebook']);
Route::put('tentang/twitter/{id}', [FrontendController::class, 'tentang_count_twitter']);
Route::put('tentang/whatsapp/{id}', [FrontendController::class, 'tentang_count_whatsapp']);

Route::get('katalog', [KatalogController::class, 'index']);
Route::get('katalog/{slug}', [KatalogController::class, 'detail']);

Route::get('login', [FrontendController::class, 'login']);
Route::post('login', [FrontendController::class, 'post_login']);
Route::get('registrasi', [FrontendController::class, 'registration']);
Route::post('registrasi', [FrontendController::class, 'post_registration']);
Route::post('logout-member', [FrontendController::class, 'logout']);
Route::get('profil', [FrontendController::class, 'profile']);
Route::put('profil', [FrontendController::class, 'put_profile']);
Route::put('change-password', [FrontendController::class, 'put_password']);

Route::get('keranjang', [KeranjangController::class, 'index']);
Route::post('order', [KeranjangController::class, 'order']);

Route::get('/auth', [AuthController::class, 'index'])->name('auth');
Route::post('/auth', [AuthController::class, 'authenticate']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::prefix('admin')->middleware(['auth:admin'])->group(function () {
    Route::get('/', [Admin\DashboardController::class, 'index'])->name('admin.index');
    Route::put('settings/update-logo', [Admin\SettingController::class, 'update_logo'])->middleware('can:isAllow');
    Route::put('settings/update-hero', [Admin\SettingController::class, 'update_hero'])->middleware('can:isAllow');
    Route::post('settings/update-time', [Admin\SettingController::class, 'update_time'])->middleware('can:isAllow');
    Route::post('settings/delete-time', [Admin\SettingController::class, 'delete_time'])->middleware('can:isAllow');
    Route::post('settings/update-payment-method', [Admin\SettingController::class, 'update_paymentMethod'])->middleware('can:isAllow');
    Route::post('settings/delete-payment-method', [Admin\SettingController::class, 'delete_paymentMethod'])->middleware('can:isAllow');
    Route::put('settings/update-pinpoint', [Admin\SettingController::class, 'update_pinpoint'])->middleware('can:isAllow');
    Route::put('settings/update-socmed', [Admin\SettingController::class, 'update_socmed'])->middleware('can:isAllow');
    Route::put('settings/update-sell-method', [Admin\SettingController::class, 'update_sell'])->middleware('can:isAllow');
    Route::resource('settings', Admin\SettingController::class)->middleware('can:isAllow');
    Route::get('roles/select2', [Admin\RoleController::class, 'select2']);
    Route::resource('roles', Admin\RoleController::class)->middleware('can:isAllow');
    Route::post('users/resetpassword', [Admin\UserController::class, 'resetpassword'])->middleware('can:isAllow');
    Route::resource('users', Admin\UserController::class)->middleware('can:isAllow');
    Route::get('profile', [Admin\ProfileController::class, 'index']);
    Route::put('profile/{id}', [Admin\ProfileController::class, 'update']);
    Route::get('profile/edit', [Admin\ProfileController::class, 'edit']);
    Route::get('profile/edit-password', [Admin\ProfileController::class, 'edit_password']);
    Route::put('profile/change-password/{id}', [Admin\ProfileController::class, 'change_password']);
    Route::get('produk/category/select2', [Admin\CategoryController::class, 'select2_produk']);
    Route::post('produk/uploadimage', [Admin\ProdukController::class, 'uploadimage'])->middleware('can:isAllow')->name('produk.uploadimage');
    Route::post('produk/deleteimage', [Admin\ProdukController::class, 'deleteimage'])->middleware('can:isAllow')->name('produk.deleteimage');
    Route::get('produk/image', [Admin\ProdukController::class, 'getImage'])->middleware('can:isAllow')->name('produk.getimage');
    Route::resource('produk', Admin\ProdukController::class)->middleware('can:isAllow');
    Route::get('tentang', [Admin\TentangController::class, 'index'])->middleware('can:isAllow');
    Route::put('tentang/{id}', [Admin\TentangController::class, 'update'])->middleware('can:isAllow');
    Route::get('penjualan', [Admin\PenjualanController::class, 'index'])->middleware('can:isAllow');
    Route::get('penjualan/check-booked', [Admin\PenjualanController::class, 'check_booked'])->middleware('can:isAllow');
    Route::get('penjualan/{id}', [Admin\PenjualanController::class, 'get_data'])->middleware('can:isAllow');
    Route::post('penjualan/update-status', [Admin\PenjualanController::class, 'update_status'])->middleware('can:isAllow');
    Route::get('members', [Admin\MemberController::class, 'index'])->middleware('can:isAllow');
    Route::get('members/{id}', [Admin\MemberController::class, 'get_data'])->middleware('can:isAllow');
    Route::post('members/update-status', [Admin\MemberController::class, 'update_status'])->middleware('can:isAllow');
});
