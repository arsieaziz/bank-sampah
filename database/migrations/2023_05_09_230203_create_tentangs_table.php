<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tentangs', function (Blueprint $table) {
            $table->id();
            $table->text('value');
            $table->string('view')->default(0);
            $table->string('facebook')->default(0);
            $table->string('twitter')->default(0);
            $table->string('whatsapp')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tentangs');
    }
};
