<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\FasilitasUmum;
use App\Models\Instansi;
use App\Models\JenisPerumahan;
use App\Models\Role;
use App\Models\Rtlh;
use App\Models\User;
use App\Models\Kawasan;
use App\Models\Setting;
use App\Models\Pekerjaan;
use App\Models\Pendidikan;
use App\Models\Penghasilan;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        User::factory(1)->create();

        Setting::create([
            'name' => 'web_title',
            'description' => 'Web Title',
            'type' => 'text',
            'value' => 'Bank Sampah',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'web_description',
            'description' => 'Web Description',
            'type' => 'textarea',
            'value' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe iste laboriosam asperiores ipsam recusandae nulla beatae laborum voluptatem eum hic ipsum animi quae veritatis tempora aut, ad, quia harum qui!',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'web_keyword',
            'description' => 'Web Keyword',
            'type' => 'text',
            'value' => 'Website, Apps, Bank Sampah',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'address',
            'description' => 'Address',
            'type' => 'textarea',
            'value' => 'Kel Talang, Kec. Telukbetung Selatan, Kota Bandar Lampung, Lampung',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'telp',
            'description' => 'Telepon',
            'type' => 'text',
            'value' => '(0721) 781740',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'email',
            'description' => 'Email',
            'type' => 'text',
            'value' => 'admin@gmail.com',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'instagram',
            'description' => 'Instagram',
            'type' => 'text',
            'value' => 'arsieaziz',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'latlong',
            'description' => 'Latitude & Longitude',
            'type' => 'text',
            'value' => '-5.3573825,105.2668444',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'image',
            'description' => 'Image',
            'type' => 'logo',
            'value' => 'logo.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'text_first',
            'description' => '#000000',
            'type' => 'logo',
            'value' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        Setting::create([
            'name' => 'text_second',
            'description' => '#000000',
            'type' => 'logo',
            'value' => '',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
