<?php

return [
    'name' => 'Mudahsaja.id - Toko Online',
    'manifest' => [
        'name' => env('APP_NAME', 'Toko Online'),
        'short_name' => 'PWA',
        'start_url' => './',
        'background_color' => '#ffffff',
        'theme_color' => '#000000',
        'display' => 'standalone',
        'orientation' => 'any',
        'status_bar' => 'black',
        'icons' => [
            '72x72' => [
                'path' => '/images/favicon/icon-72x72.png',
                'purpose' => 'any'
            ],
            '96x96' => [
                'path' => '/images/favicon/icon-96x96.png',
                'purpose' => 'any'
            ],
            '128x128' => [
                'path' => '/images/favicon/icon-128x128.png',
                'purpose' => 'any'
            ],
            '144x144' => [
                'path' => '/images/favicon/icon-144x144.png',
                'purpose' => 'any'
            ],
            '152x152' => [
                'path' => '/images/favicon/icon-152x152.png',
                'purpose' => 'any'
            ],
            '192x192' => [
                'path' => '/images/favicon/icon-192x192.png',
                'purpose' => 'any'
            ],
            '310x310' => [
                'path' => '/images/favicon/icon-384x384.png',
                'purpose' => 'any'
            ],
            '512x512' => [
                'path' => '/images/favicon/icon-512x512.png',
                'purpose' => 'any'
            ],
            '512x512' => [
                'path' => '/images/favicon/mask-196x196.png',
                'purpose' => 'any maskable'
            ],
        ],
        'splash' => [
            '240x320' => '/images/favicon/splash-240x320.png',
            '320x480' => '/images/favicon/splash-320x480.png',
            '480x800' => '/images/favicon/splash-480x800.png',
            '720x1280' => '/images/favicon/splash-720x1280.png',
            '960x1600' => '/images/favicon/splash-960x1600.png',
            '1280x1920' => '/images/favicon/splash-1280x1920.png'
        ],
        'shortcuts' => [
            [
                'name' => 'Mudahsaja.id',
                'description' => 'Dengan Mudahsaja.id sekarang semua bisa punya toko online sendiri!',
                'url' => '/',
                'icons' => [
                    "src" => "/images/favicon/icon-72x72.png",
                    "purpose" => "any"
                ]
            ],
            [
                'name' => 'Mudahsaja.id - Katalog',
                'description' => 'Katalog Mudahsaja.id memudahkan pembeli untuk mencari barang yang sesuai dengan apa yang di inginkan',
                'url' => '/katalog'
            ]
        ],
        'custom' => []
    ]
];
