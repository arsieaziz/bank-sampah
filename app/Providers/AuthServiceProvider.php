<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAllow', function($user) {
            $link = request()->segment(2);
            $return = TRUE;
            if($user->role_id != '0'){
                $res = User::with('roles')->where('id',$user->id)->first();
                $role = json_decode($res->roles->access,TRUE);
                if(!in_array($link,$role) && !empty($link)){
                    $return = FALSE;
                }
            }
            return $return;
        });
    }
}
