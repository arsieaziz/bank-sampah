<?php

namespace App\Http\Controllers\Admin;

use App\Models\Tentang;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class TentangController extends Controller {
    public function index() {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Users',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
            ['disabled' => true, 'url' => '#', 'title' => 'Tentang'],
        ];
        $data = Tentang::first();
        return view('admin.tentang', compact('config', 'breadcrumbs', 'data'));
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'value' => 'required'
        ]);

        $data = Tentang::findOrFail($id);
        if (!$validator->fails()) {
            $req = $validator->safe()->all();
            DB::beginTransaction();
            try {
                $data->update($req);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => 'admin/tentang']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }
}
