<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Helpers\FileUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller {

	public function index() {
		$data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
		$config = [
			'title' => $data[0]->value . ' - Pengaturan',
			'description' => $data[1]->value
		];
		$breadcrumbs = [
			['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
			['disabled' => true, 'url' => '#', 'title' => 'Pengaturan'],
		];
		$data = Setting::whereIn('type', ['text', 'textarea'])->get();
		$pinpoint = Setting::where('type', 'pinpoint')->pluck('value');
		$color = Setting::where('type', 'color')->pluck('value');
		$logo = [];
		foreach (Setting::where('type', 'logo')->get() as $item) {
			$get[$item['name']]['value'] = $item['value'];
			$get[$item['name']]['description'] = $item['description'];
			$logo = $get;
		}
		$hero = [];
		foreach (Setting::where('type', 'hero')->get() as $item) {
			$hero[$item['name']] = $item['value'];
		}
		$socmed = [];
		foreach (Setting::where('type', 'socmed')->get() as $item) {
			$socmed[$item['name']] = $item['value'];
		}
		$method = [];
		foreach (Setting::where('type', 'method')->get() as $item) {
			$get[$item['name']]['value'] = $item['value'];
			$get[$item['name']]['description'] = $item['description'];
			$method = $get;
		}
		$discount = [];
		foreach (Setting::where('type', 'discount')->get() as $item) {
			$get[$item['name']]['value'] = $item['value'];
			$get[$item['name']]['description'] = $item['description'];
			$discount = $get;
		}
		$time = Setting::where('type', 'open_time')->get();
		$courier = Setting::where('type', 'courier')->get();
		$payment_method = Setting::where('type', 'payment_method')->get();
		return view('admin.settings', compact('config', 'breadcrumbs', 'data', 'logo', 'hero', 'time', 'pinpoint', 'color', 'socmed', 'courier', 'method', 'payment_method', 'discount'));
	}

	public function store(Request $request) {
		$validator = Validator::make($request->all(), []);
		if (!$validator->fails()) {
			DB::beginTransaction();
			try {
				$data = Setting::findOrFail($request['pk']);
				$data->update([
					'value' => $request['value'],
				]);
				DB::commit();
				$response = response()->json(['status' => 'success', 'message' => 'Data has been save', 'redirect' => '/settings']);
			} catch (\Throwable $throw) {
				DB::rollBack();
				Log::error($throw);
				$response = response()->json(['error' => $throw->getMessage()]);
			}
		} else {
			$response = response()->json(['error' => $validator->errors()]);
		}
		return $response;
	}

	public function update_logo(Request $request) {
		DB::beginTransaction();
		$old = Setting::where([['type', '=', 'logo'], ['name', '=', 'image']])->get();
		$old_image = $old[0]->value;
		try {
			if (isset($request['image']) && !empty($request['image'])) {
				Storage::disk('public')->delete(["images/original/$old_image", "images/thumbnail/$old_image"]);
				$img = isset($request['image']) && !empty($request['image']) ? FileUpload::uploadImageWithoutOriginal('image', '[]') : NULL;
				Setting::where([['type', '=', 'logo'], ['name', '=', 'image']])->update(['value' => $img]);
			}
			Setting::where([['type', '=', 'logo'], ['name', '=', 'text_first']])->update(['description' => $request['color_first'], 'value' => $request['text_first']]);
			Setting::where([['type', '=', 'logo'], ['name', '=', 'text_second']])->update(['description' => $request['color_second'], 'value' => $request['text_second']]);
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}

	public function update_color(Request $request) {
		DB::beginTransaction();
		try {
			if (isset($request['color_pallete']) && !empty($request['color_pallete'])) {
				Setting::where('type', 'color')->update(['value' => $request['color_pallete']]);
			}
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}

	public function update_hero(Request $request) {
		DB::beginTransaction();
		$old = Setting::where([['type', '=', 'hero'], ['name', '=', 'image']])->get();
		$old_image = $old[0]->value;
		try {
			if (isset($request['image']) && !empty($request['image'])) {
				$dimensions = [array('1920', '1080', 'thumbnail')];
				Storage::disk('public')->delete(["images/original/$old_image", "images/thumbnail/$old_image"]);
				$img = isset($request['image']) && !empty($request['image']) ? FileUpload::uploadImage('image', $dimensions) : NULL;
				Setting::where([['type', '=', 'hero'], ['name', '=', 'image']])->update(['value' => $img]);
			}
			if (isset($request['title']) && !empty($request['title'])) {
				Setting::where([['type', '=', 'hero'], ['name', '=', 'title']])->update(['value' => $request['title']]);
			}
			if (isset($request['description']) && !empty($request['description'])) {
				Setting::where([['type', '=', 'hero'], ['name', '=', 'description']])->update(['value' => $request['description']]);
			}
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}

	public function update_time(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'description' => 'required',
		]);
		if (!$validator->fails()) {
			$data = $validator->safe()->all();
			$data['type'] = 'open_time';
			$data['value'] = '';
			DB::beginTransaction();
			try {
				Setting::create($data);
				DB::commit();
				$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings	']);
			} catch (\Throwable $throw) {
				DB::rollBack();
				Log::error($throw);
				$response = response()->json(['error' => $throw->getMessage()]);
			}
		} else {
			$response = response()->json(['error' => $validator->errors()]);
		}
		return $response;
	}

	public function delete_time(Request $request) {
		$data = Setting::findOrFail($request->id);
		if ($data->delete()) {
			$response = response()->json(['status' => 'success', 'message' => 'Data has been delete']);
		} else {
			$response = response()->json(['status' => 'failed', 'message' => 'Data cant delete']);
		}
		return $response;
	}

	public function update_pinpoint(Request $request) {
		DB::beginTransaction();
		try {
			if (isset($request['pinpoint']) && !empty($request['pinpoint'])) {
				Setting::where('type', 'pinpoint')->update(['value' => $request['pinpoint']]);
			}
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}

	public function update_socmed(Request $request) {
		DB::beginTransaction();
		try {
			if (isset($request['whatsapp']) && !empty($request['whatsapp'])) {
				Setting::where([['name', 'whatsapp'], ['type', 'socmed']])->update(['value' => $request['whatsapp']]);
			}
			if (isset($request['facebook']) && !empty($request['facebook'])) {
				Setting::where([['name', 'facebook'], ['type', 'socmed']])->update(['value' => $request['facebook']]);
			}
			if (isset($request['instagram']) && !empty($request['instagram'])) {
				Setting::where([['name', 'instagram'], ['type', 'socmed']])->update(['value' => $request['instagram']]);
			}
			if (isset($request['twitter']) && !empty($request['twitter'])) {
				Setting::where([['name', 'twitter'], ['type', 'socmed']])->update(['value' => $request['twitter']]);
			}
			if (isset($request['tiktok']) && !empty($request['tiktok'])) {
				Setting::where([['name', 'tiktok'], ['type', 'socmed']])->update(['value' => $request['tiktok']]);
			}
			if (isset($request['youtube']) && !empty($request['youtube'])) {
				Setting::where([['name', 'youtube'], ['type', 'socmed']])->update(['value' => $request['youtube']]);
			}
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}

	public function update_courier(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
		]);
		if (!$validator->fails()) {
			$data = $validator->safe()->all();
			$data['type'] = 'courier';
			$data['description'] = '';
			$data['value'] = '';
			DB::beginTransaction();
			try {
				Setting::create($data);
				DB::commit();
				$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings	']);
			} catch (\Throwable $throw) {
				DB::rollBack();
				Log::error($throw);
				$response = response()->json(['error' => $throw->getMessage()]);
			}
		} else {
			$response = response()->json(['error' => $validator->errors()]);
		}
		return $response;
	}

	public function delete_courier(Request $request) {
		$data = Setting::findOrFail($request->id);
		if ($data->delete()) {
			$response = response()->json(['status' => 'success', 'message' => 'Data has been delete']);
		} else {
			$response = response()->json(['status' => 'failed', 'message' => 'Data cant delete']);
		}
		return $response;
	}

	public function update_paymentMethod(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'description' => 'required',
			'value' => 'required',
		]);
		if (!$validator->fails()) {
			$data = $validator->safe()->all();
			$data['type'] = 'payment_method';
			DB::beginTransaction();
			try {
				Setting::create($data);
				DB::commit();
				$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings	']);
			} catch (\Throwable $throw) {
				DB::rollBack();
				Log::error($throw);
				$response = response()->json(['error' => $throw->getMessage()]);
			}
		} else {
			$response = response()->json(['error' => $validator->errors()]);
		}
		return $response;
	}

	public function delete_paymentMethod(Request $request) {
		$data = Setting::findOrFail($request->id);
		if ($data->delete()) {
			$response = response()->json(['status' => 'success', 'message' => 'Data has been delete']);
		} else {
			$response = response()->json(['status' => 'failed', 'message' => 'Data cant delete']);
		}
		return $response;
	}

	public function update_sell(Request $request) {
		DB::beginTransaction();
		try {
			if (isset($request['metode_pemesanan']) && !empty($request['metode_pemesanan'])) {
				Setting::where([['name', 'metode_pemesanan'], ['type', 'method']])->update(['value' => $request['metode_pemesanan']]);
			} else {
				Setting::where([['name', 'metode_pemesanan'], ['type', 'method']])->update(['value' => '0']);
			}
			if (isset($request['opsi_pemesanan']) && !empty($request['opsi_pemesanan'])) {
				Setting::where([['name', 'opsi_pemesanan'], ['type', 'method']])->update(['value' => $request['opsi_pemesanan']]);
			} else {
				Setting::where([['name', 'opsi_pemesanan'], ['type', 'method']])->update(['value' => '0']);
			}
			if (isset($request['pilihan_waktu']) && !empty($request['pilihan_waktu'])) {
				Setting::where([['name', 'pilihan_waktu'], ['type', 'method']])->update(['value' => $request['pilihan_waktu'], 'description' => $request['pilihan_tipe_waktu']]);
			} else {
				Setting::where([['name', 'pilihan_waktu'], ['type', 'method']])->update(['value' => '0']);
			}
			if (isset($request['titik_lokasi_pelanggan']) && !empty($request['titik_lokasi_pelanggan'])) {
				Setting::where([['name', 'titik_lokasi_pelanggan'], ['type', 'method']])->update(['value' => $request['titik_lokasi_pelanggan']]);
			} else {
				Setting::where([['name', 'titik_lokasi_pelanggan'], ['type', 'method']])->update(['value' => '0']);
			}
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}

	public function update_discount(Request $request) {
		DB::beginTransaction();
		try {
			Setting::where([['name', 'discount_percent'], ['type', 'discount']])->update(['value' => $request['discount'], 'description' => $request['discountRowOptions']]);
			DB::commit();
			$response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/settings']);
		} catch (\Throwable $throw) {
			DB::rollBack();
			Log::error($throw);
			$response = response()->json(['error' => $throw->getMessage()]);
		}
		return $response;
	}
}
