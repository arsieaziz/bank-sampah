<?php

namespace App\Http\Controllers\Admin;

use App\Models\Member;
use App\Models\Setting;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller {
    public function index(Request $request) {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Users',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
            ['disabled' => true, 'url' => '#', 'title' => 'Members'],
        ];

        if ($request->ajax()) {
            $active = $request['active'];
            $data = Member::when($active, function ($query, $active) {
                if ($active == 'not') {
                    return $query->where('active', '0');
                } else if ($active == 'yes') {
                    return $query->where('active', '1');
                }
            });
            return DataTables::of($data)->addIndexColumn()->editColumn('created_at', function ($row) {
                return date_format($row->created_at, "d F Y H:i:s");
            })->addColumn('action', function ($row) {
                $actionBtn = '<a href="#" class="btn btn-sm btn-outline-primary" data-bs-toggle="modal" data-bs-target="#modalView" data-id="' . $row->id . '">Lihat / Update</a>';
                return $actionBtn;
            })->rawColumns(['action'])->make(true);
        }
        return view('admin.member.index', compact('config', 'breadcrumbs'));
    }
    public function get_data($id) {
        $data = Member::findOrFail($id);
        $data->date_birth = date('d F Y', strtotime($data->date_birth));
        $data['sejak'] = date('d F Y', strtotime($data->created_at));
        $data->number_phone = '62' . ltrim($data->number_phone, '0');
        if ($data->active == '0') {
            $data['status'] = '<div class="btn btn-sm btn-danger">Tidak Aktif</div>';
        } else {
            $data['status'] = '<div class="btn btn-sm btn-success">Aktif</div>';
        }
        return $data;
    }
    public function update_status(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);
        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $member = Member::findOrFail($data['id']);
            DB::beginTransaction();
            try {
                $member->update(['active' => $data['status']]);
                DB::commit();
                $response = response()->json(['message' => 'Data has been upate', 'redirect' => '/admin/members']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }
}
