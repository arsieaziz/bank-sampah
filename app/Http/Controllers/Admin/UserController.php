<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Setting;
use App\Helpers\FileUpload;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller{

    public function index(Request $request){
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value.' - Users',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
			['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
        	['disabled' => true, 'url' => '#', 'title' => 'Users'],
        ];

        if($request->ajax()){
            $active = $request['active'];
            $data = User::with('roles')->where('role_id', '!=', '0')->when($active, function ($query, $active) {
                if ($active == 'non_active') {
                    return $query->where('active', '0');
                } else {
                    return $query->where('active', '1');
                }
            });
            return DataTables::of($data)->addIndexColumn()->addColumn('action', function ($row) {
                $actionBtn = '<div class="dropdown">
                <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog fa-fw"></i> Aksi</button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="admin/users/' . $row->id . '/edit">Ubah</a></li>
                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#modalReset" data-bs-id="' . $row->id . '" class="dropdown-item">Reset Password</a></li>
                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-bs-id="' . $row->id . '" class="delete dropdown-item">Hapus</a></li>
                        </ul>
                    </div> ';
                return $actionBtn;
            })->rawColumns(['action'])->make(true);
        }
        return view('admin.users.index', compact('config', 'breadcrumbs'));
    }

    public function create(){
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value.' - Add Users',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
			['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
			['disabled' => false, 'url' => '/admin/users', 'title' => 'Users'],
        	['disabled' => true, 'url' => '#', 'title' => 'Add Users'],
        ];
        $mode = 'create';
        return view('admin.users.form', compact('config', 'breadcrumbs','mode'));
    }

    public function edit($id){
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value.' - Edit Users',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
			['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
			['disabled' => false, 'url' => '/admin/users', 'title' => 'Users'],
        	['disabled' => true, 'url' => '#', 'title' => 'Edit Users'],
        ];
        $data = User::with('roles')->findOrFail($id);
        $mode = 'edit';
        return view('admin.users.form', compact('config', 'breadcrumbs','data','mode'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'position' => 'required|max:255',
            'role_id' => 'required',
            'username' => 'required|min:3|max:255|unique:users,username',
            'email' => 'required|email:dns|unique:users,email',
            'password' => 'required|min:5|max:255|confirmed',
            'password_confirmation'  => 'nullable|same:password',
            'active' => 'required|between:0,1',
            'image' => 'image|mimes:jpg,png,jpeg',
        ],[
            'same' => ':attribute and :other must match.',
            'unique' => ':attribute is not available'
        ]);
        
        if (!$validator->fails()) {
            $data = $validator->safe()->except(['password_confirmation']);
            $data['password'] = Hash::make($data['password']);
            $dimensions = [array('300', '300', 'thumbnail')];
            DB::beginTransaction();
            try {
                $img = isset($request->image) && !empty($request->image) ? FileUpload::uploadImage('image', $dimensions) : NULL;
                $data['image'] = $img;
                $data['email_verified_at'] = now();
                User::create($data);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/users']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(),[
            'name' => 'required|max:255',
            'position' => 'required|max:255',
            'role_id' => 'required',
            'username' => 'required|min:3|max:255|unique:users,username,'.$id,
            'email' => 'required|email:dns|unique:users,email,'.$id,
            'password' => 'nullable',
            'password_confirmation'  => 'nullable|same:password',
            'active' => 'required|between:0,1',
            'image' => 'nullable|image|mimes:jpg,png,jpeg',
        ],[
            'same' => ':attribute and :other must match.',
            'unique' => ':attribute is not available'
        ]);

        $data = User::findOrFail($id);
        if (!$validator->fails()) {
            DB::beginTransaction();
            try {
                $data->update([
                    'role_id' => $request['role_id'],
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'username' => $request['username'],
                    'active' => $request['active']
                ]);
                if (isset($request['image']) && !empty($request['image'])){
                    $dimensions = [array('300', '300', 'thumbnail')];
                    Storage::disk('public')->delete(["images/original/$data->image", "images/thumbnail/$data->image"]);
                    $img = isset($request->image) && !empty($request->image) ? FileUpload::uploadImage('image', $dimensions) : NULL;
                    $data->update([
                        'image' => $img
                    ]);
                }
                if(isset($request['password']) && !empty($request['password'])){
                    $password = Hash::make($request['password']);
                    $data->update([
                        'password' => $password
                    ]);
                }
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/users']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function destroy($id){
        $data = User::findOrFail($id);
        if($data->delete()){
            Storage::disk('public')->delete(["images/original/$data->image", "images/thumbnail/$data->image"]);
            $response = response()->json(['status' => 'success','message' => 'Data has been delete']);
        }else{
            $response = response()->json(['status' => 'failed','message' => 'Data cant delete']);
        }
        return $response;
    }

    public function resetpassword(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if (!$validator->fails()) {
            $data = User::findOrFail($request->id);
            $req['password'] = Hash::make($data['username']);
            DB::beginTransaction();
            try {
                $data->update($req);
                DB::commit();
                $response = response()->json(['message' => 'Password has been reset', 'redirect' => '/admin/users']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }
}