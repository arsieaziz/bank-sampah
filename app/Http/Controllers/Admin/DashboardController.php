<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sale;
use App\Models\Produk;
use App\Models\Setting;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Cabang;
use App\Models\Member;

class DashboardController extends Controller {
    public function index() {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Dashboard',
            'description' => $data[1]->value
        ];
        $getSaleToday = Sale::where('status', 'done')->whereDate('updated_at', Carbon::today())->get();
        $saleToday['jumlah_pesanan'] = 0;
        $saleToday['jumlah_produk'] = 0;
        $saleToday['jumlah_penjualan'] = 0;
        if (!empty($getSaleToday)) {
            foreach ($getSaleToday as $key => $value) {
                $product = json_decode($value['product'], TRUE);
                foreach ($product as $k => $val) {
                    $saleToday['jumlah_produk'] = $saleToday['jumlah_produk'] + $val['qty'];
                }
                $saleToday['jumlah_penjualan'] = $saleToday['jumlah_penjualan'] + $value['total_harga'];
                $saleToday['jumlah_pesanan'] = $saleToday['jumlah_pesanan'] + 1;
            }
        }
        $memberSale = DB::select("SELECT `m`.`fullname`,`m`.`number_phone`,`m`.`created_at`,`m`.`active`, count(`s`.`id`) AS `total` FROM `sales` AS `s` LEFT JOIN `members` AS `m` ON `m`.`id`=`s`.`member_id` WHERE `s`.`member_id`!='0' AND `s`.`status`='done' GROUP BY `s`.`member_id` ORDER BY `total` DESC LIMIT 10");
        $count['member'] = Member::where('active', '1')->count();
        $count['produk'] = Produk::where('active', '1')->count();
        $product['new'] = Produk::orderBy('created_at', 'DESC')->limit(10)->get();
        $product['sale'] = Produk::orderBy('sale', 'DESC')->limit(10)->get();
        $graphic = Produk::get()->toArray();
        $product['chart_name'] = array();
        $product['chart_sale'] = array();
        foreach ($graphic as $key => $value) {
            array_push($product['chart_name'], $value['name']);
            array_push($product['chart_sale'], $value['sale']);
        };
        return view('admin.dashboard', compact('config', 'saleToday', 'memberSale', 'count', 'product'));
    }
}
