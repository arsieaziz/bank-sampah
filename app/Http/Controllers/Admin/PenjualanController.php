<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sale;
use App\Models\Produk;
use App\Models\Setting;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PenjualanController extends Controller {
    public function index(Request $request) {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Users',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
            ['disabled' => true, 'url' => '#', 'title' => 'Users'],
        ];

        if ($request->ajax()) {
            $status = $request['status'];
            $data = Sale::with('membership')->when($status, function ($query, $status) {
                if (!empty($status)) {
                    if ($status == 'booked') {
                        return $query->where('status', 'booked');
                    } elseif ($status == 'cancel') {
                        return $query->where('status', 'cancel');
                    } else {
                        return $query->where('status', 'done');
                    }
                }
            });
            return DataTables::of($data)->addIndexColumn()->editColumn('name', function ($row) {
                return urldecode($row->membership->fullname);
            })->editColumn('total_pay', function ($row) {
                return 'Rp ' . number_format($row->total_harga, 0, ',', '.');
            })->editColumn('created_at', function ($row) {
                return date_format($row->created_at, "d-m-Y H:i:s");
            })->addColumn('action', function ($row) {
                if ($row->status != 'booked') {
                    $text = '<i class="fas fa-eye"></i> Lihat';
                } else {
                    $text = 'Update';
                }
                $actionBtn = '<a href="#" class="btn btn-sm btn-outline-primary" data-bs-toggle="modal" data-bs-target="#modalUpdate" data-id="' . $row->id . '">' . $text . '</a>';
                return $actionBtn;
            })->rawColumns(['action'])->make(true);
        }
        return view('admin.penjualan.index', compact('config', 'breadcrumbs'));
    }
    public function get_data($id) {
        $data = Sale::with('membership')->findOrFail($id);
        return $data;
    }
    public function update_status(Request $request) {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'status' => 'required',
        ]);
        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $sale = Sale::findOrFail($data['id']);
            DB::beginTransaction();
            try {
                if ($data['status'] == 'cancel') {
                    foreach (json_decode($sale->product, TRUE) as $key => $val) {
                        $product = Produk::where('id', $val['id'])->firstOrFail();
                        $product->booked = $product->booked - $val['qty'];
                        $product->save();
                    }
                } elseif ($data['status'] == 'done') {
                    foreach (json_decode($sale->product, TRUE) as $key => $val) {
                        $product = Produk::where('id', $val['id'])->firstOrFail();
                        $product->booked = $product->booked - $val['qty'];
                        $product->sale = $product->sale + $val['qty'];
                        $product->save();
                    }
                }
                $sale->update(['status' => $data['status']]);
                DB::commit();
                $response = response()->json(['message' => 'Data has been upate', 'redirect' => '/admin/penjualan']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }
    public function check_booked() {
        $data = Sale::where('status', 'booked')->count();
        return $data;
    }
}
