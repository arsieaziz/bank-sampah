<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\Setting;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{

    public function index(Request $request)
    {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Roles',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
            ['disabled' => false, 'url' => '/admin/users', 'title' => 'Users'],
            ['disabled' => true, 'url' => '#', 'title' => 'Roles'],
        ];

        if ($request->ajax()) {
            $data = Role::all();
            return DataTables::of($data)->addIndexColumn()->editColumn('access', function ($row) {
                $rows = json_decode($row->access);
                $access = '<ul>';
                foreach ($rows as $val) {
                    $access .= '<li>' . ucwords($val) . '</li>';
                }
                $access .= '</ul>';
                return $access;
            })->addColumn('action', function ($row) {
                $actionBtn = '<div class="dropdown">
                            <button type="button" class="btn btn-sm btn-outline-primary dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog fa-fw"></i> Aksi</button>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="admin/roles/' . $row->id . '/edit">Ubah</a></li>
                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-bs-id="' . $row->id . '" class="delete dropdown-item">Hapus</a></li>
                        </ul>
                    </div> ';
                return $actionBtn;
            })->rawColumns(['action', 'access'])->make(true);
        }
        return view('admin.roles.index', compact('config', 'breadcrumbs'));
    }

    public function create()
    {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Add Roles',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
            ['disabled' => false, 'url' => '/admin/users', 'title' => 'Users'],
            ['disabled' => false, 'url' => '/admin/roles', 'title' => 'Roles'],
            ['disabled' => true, 'url' => '#', 'title' => 'Add Roles'],
        ];
        return view('admin.roles.form', compact('config', 'breadcrumbs'));
    }

    public function edit($id)
    {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Edit Roles',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => '/admin', 'title' => 'Dashboard'],
            ['disabled' => false, 'url' => '/admin/users', 'title' => 'Users'],
            ['disabled' => false, 'url' => '/admin/roles', 'title' => 'Roles'],
            ['disabled' => true, 'url' => '#', 'title' => 'Edit Roles'],
        ];
        $data = Role::findOrFail($id);
        $data['access'] = json_decode($data['access'], TRUE);
        return view('admin.roles.form', compact('config', 'breadcrumbs', 'data'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:roles,name',
            'akses' => 'required'
        ]);

        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $data['slug'] = str()->slug($data['name']);
            $data['access'] = json_encode($data['akses'], TRUE);
            DB::beginTransaction();
            try {
                Role::create($data);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/roles']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:roles,name,' . $id,
            'akses' => 'required'
        ]);

        $data = Role::findOrFail($id);
        if (!$validator->fails()) {
            $req = $validator->safe()->all();
            $req['slug'] = str()->slug($req['name']);
            $req['access'] = json_encode($req['akses'], TRUE);
            DB::beginTransaction();
            try {
                $data->update($req);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => '/admin/roles']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function destroy($id)
    {
        $data = Role::findOrFail($id);
        if ($data->delete()) {
            $response = response()->json(['status' => 'success', 'message' => 'Data has been delete']);
        } else {
            $response = response()->json(['status' => 'failed', 'message' => 'Data cant delete']);
        }
        return $response;
    }

    public function select2(Request $request)
    {
        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;
        $data = Role::where('name', 'LIKE', '%' . $request->q . '%')
            ->orderBy('name')
            ->when($request['idArray'], function ($query, $role) use ($request) {
                return $query->whereIn('id', $request['idArray']);
            })
            ->skip($offset)
            ->take($resultCount)
            ->selectRaw('id, name as text')
            ->get();

        $count = Role::where('name', 'LIKE', '%' . $request->q . '%')
            ->get()
            ->count();

        $endCount = $offset + $resultCount;
        $morePages = $count > $endCount;

        $results = array(
            "results" => $data,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return response()->json($results);
    }
}
