<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\Produk;
use App\Models\Photo;
use App\Helpers\FileUpload;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProdukController extends Controller {
    public function index(Request $request) {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Produk Sampah',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => 'admin', 'title' => 'Dashboard'],
            ['disabled' => true, 'url' => '#', 'title' => 'Produk Sampah'],
        ];
        $data = Produk::where([
            ['name', '!=', Null],
            [function ($query) use ($request) {
                if (($s = $request->s)) {
                    $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
                }
            }]
        ])->paginate(6);
        return view('admin.produk.index', compact('config', 'breadcrumbs', 'data'));
    }

    public function create() {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Tambah Produk Sampah',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => 'admin', 'title' => 'Dashboard'],
            ['disabled' => false, 'url' => 'admin/produk', 'title' => 'Data Produk Sampah'],
            ['disabled' => true, 'url' => '#', 'title' => 'Tambah Produk Sampah'],
        ];
        $mode = 'create';
        return view('admin.produk.form', compact('config', 'breadcrumbs', 'mode'));
    }

    public function edit($id) {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        $config = [
            'title' => $data[0]->value . ' - Edit Data Produk Sampah',
            'description' => $data[1]->value
        ];
        $breadcrumbs = [
            ['disabled' => false, 'url' => 'admin', 'title' => 'Dashboard'],
            ['disabled' => false, 'url' => 'admin/produk', 'title' => 'Data Produk Sampah'],
            ['disabled' => true, 'url' => '#', 'title' => 'Edit Data Produk Sampah'],
        ];
        $data = Produk::findOrFail($id);
        $mode = 'edit';
        return view('admin.produk.form', compact('config', 'breadcrumbs', 'data', 'mode'));
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:produks,name',
            'price' => 'required',
            'description' => 'required',
            'main_image' => 'required',
            'active' => 'required|between:0,1'
        ]);

        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $data['price'] = str_replace('.', '', $data['price']);
            $data['slug'] = str()->slug($data['name']);
            DB::beginTransaction();
            try {
                if (isset($data['main_image']) && !empty($data['main_image'])) {
                    $dimensions = [array('500', '500', 'thumbnail')];
                    $img = isset($data['main_image']) && !empty($data['main_image']) ? FileUpload::uploadImage('main_image', $dimensions) : NULL;
                    $data['main_image'] = $img;
                }
                $produk = Produk::create($data);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => 'admin/produk/' . $produk->id . '/edit']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:produks,name,' . $id,
            'price' => 'required',
            'description' => 'required',
            'main_image' => 'nullable',
            'active' => 'required|between:0,1'
        ]);

        $data = Produk::findOrFail($id);
        if (!$validator->fails()) {
            $req = $validator->safe()->all();
            $req['price'] = str_replace('.', '', $req['price']);
            $req['slug'] = str()->slug($req['name']);
            DB::beginTransaction();
            try {
                if (isset($req['main_image']) && !empty($req['main_image'])) {
                    $dimensions = [array('500', '500', 'thumbnail')];
                    Storage::disk('public')->delete(["images/thumbnail/$data->main_image"]);
                    $img = isset($req['main_image']) && !empty($req['main_image']) ? FileUpload::uploadImage('main_image', $dimensions) : NULL;
                    $req['main_image'] = $img;
                }
                $data->update($req);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => 'admin/produk']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function destroy($id) {
        $data = Produk::findOrFail($id);
        $photo = Photo::where('parent_id', $id)->get();
        if ($data->delete()) {
            foreach ($photo as $val) {
                Storage::disk('public')->delete(["images/thumbnail/$val->value"]);
            }
            Photo::where('parent_id', $id)->delete();
            Storage::disk('public')->delete(["images/thumbnail/$data->foto"]);
            $response = response()->json(['status' => 'success', 'message' => 'Data has been delete', 'redirect' => 'admin/produk']);
        } else {
            $response = response()->json(['status' => 'failed', 'message' => 'Data cant delete']);
        }
        return $response;
    }

    public function uploadimage(Request $request) {
        DB::beginTransaction();
        try {
            $dimensions = [array('500', '500', 'thumbnail')];
            $img = isset($request->images) && !empty($request->images) ? FileUpload::uploadImage('images', $dimensions) : NULL;
            $data['parent_id'] = $request->parent_id;
            $data['value'] = $img;
            Photo::create($data);
            DB::commit();
            $response = response()->json(['message' => 'Data has been save']);
        } catch (\Throwable $throw) {
            DB::rollBack();
            Log::error($throw);
            $response = response()->json(['error' => $throw->getMessage()]);
        }
        return $response;
    }

    public function getImage(Request $request) {
        if ($request->ajax()) {
            $list = Photo::where('parent_id', '=', $request->id);
            return DataTables::of($list)->addIndexColumn()->addColumn('hapus', function ($row) {
                $actionBtn = '<a href="#" data-bs-toggle="modal" data-bs-target="#modalDelete" data-bs-id="' . $row->id . '" class="btn btn-sm btn-danger text-white"><i class="fas fa-trash"></i> Hapus</a>';
                return $actionBtn;
            })->rawColumns(['hapus'])->make(true);
        }
    }

    public function deleteimage(Request $request) {
        $data = Photo::findOrFail($request->iddelete);
        if ($data->delete()) {
            Storage::disk('public')->delete(["images/thumbnail/$data->value"]);
            $response = response()->json(['status' => 'success', 'message' => 'Data has been delete']);
        } else {
            $response = response()->json(['status' => 'failed', 'message' => 'Data cant delete']);
        }
        return $response;
    }
}
