<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\Cabang;
use App\Models\Member;
use App\Models\Produk;
use App\Models\Address;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Validator;

class KeranjangController extends Controller {
    public function index(Request $request) {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $open_time = Setting::where('type', 'open_time')->get();
        $payment = Setting::where('type', 'payment_method')->orderBy('description', 'asc')->get();
        $pinpoint = Setting::where('type', 'pinpoint')->first()->value;
        $sessionId = isset(Auth::guard('member')->user()->id) ? Auth::guard('member')->user()->id : '0';
        return view('keranjang', compact('settings', 'logo', 'hero', 'socmed', 'open_time', 'payment', 'pinpoint'));
    }

    public function order(Request $request) {
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'jumlah_produk' => 'required|max:255',
            'total_harga' => 'required|max:255',
            'catatan' => 'required|max:255',
        ]);

        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $produk = json_decode($data['product'], TRUE);
            $data['member_id'] = Auth::guard('member')->user() ? Auth::guard('member')->user()->id : '0';
            $data['status'] = 'booked';
            DB::beginTransaction();
            try {
                Sale::create($data);
                foreach ($produk as $key => $val) {
                    $data = Produk::where('id', $val['id'])->firstOrFail();
                    $data->booked = $data->booked + $val['qty'];
                    $data->save();
                }
                DB::commit();
                $response = response()->json(['message' => 'Your Order Has Been Send', 'redirect' => '/']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }
}
