<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Photo;

class KatalogController extends Controller {

    public function index(Request $request) {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $open_time = Setting::where('type', 'open_time')->get();
        $payment = Setting::where('type', 'payment_method')->orderBy('description', 'asc')->get();
        $pinpoint = Setting::where('type', 'pinpoint')->first()->value;

        if ($request->orderBy && $request->sortedBy) {
            $data = Produk::where([
                ['name', '!=', Null],
                ['active', '=', '1'],
                [function ($query) use ($request) {
                    if (($s = $request->s)) {
                        $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
                    }
                }],
            ])->orderBy($request->orderBy, $request->sortedBy)->paginate(6);
        } else {
            $data = Produk::where([
                ['name', '!=', Null],
                ['active', '=', '1'],
                [function ($query) use ($request) {
                    if (($s = $request->s)) {
                        $query->orWhere('name', 'LIKE', '%' . $s . '%')->get();
                    }
                }]
            ])->orderBy('created_at', 'desc')->paginate(8);
        }
        return view('frontend.katalog.index', compact('settings', 'logo', 'hero', 'socmed', 'open_time', 'payment', 'pinpoint', 'data'));
    }

    public function detail($slug) {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $open_time = Setting::where('type', 'open_time')->get();
        $payment = Setting::where('type', 'payment_method')->orderBy('description', 'asc')->get();
        $pinpoint = Setting::where('type', 'pinpoint')->first()->value;

        $data = Produk::where('slug', $slug)->first();
        $photo = Photo::where('parent_id', $data->id)->get();
        return view('frontend.katalog.detail', compact('settings', 'logo', 'hero', 'socmed', 'open_time', 'payment', 'pinpoint', 'data', 'photo'));
    }
}
