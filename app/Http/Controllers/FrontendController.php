<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Tentang;
use App\Models\Setting;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class FrontendController extends Controller {
    public function index(Request $request) {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $produk_terbaru = Produk::where('active', '1')->orderBy('created_at', 'desc')->limit(12)->get();
        $open_time = Setting::where('type', 'open_time')->get();
        $payment = Setting::where('type', 'payment_method')->orderBy('description', 'asc')->get();
        $pinpoint = Setting::where('type', 'pinpoint')->first()->value;
        return view('index', compact('settings', 'logo', 'hero', 'socmed', 'produk_terbaru', 'open_time', 'payment', 'pinpoint'));
    }

    public function offline() {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $settings['web_title'] = $settings['web_title'] . ' - Login';
        return view('vendor.laravelpwa.offline', compact('settings', 'logo', 'socmed'));
    }

    public function tentang() {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $open_time = Setting::where('type', 'open_time')->get();
        $payment = Setting::where('type', 'payment_method')->orderBy('description', 'asc')->get();
        $pinpoint = Setting::where('type', 'pinpoint')->first()->value;
        $data = Tentang::first();
        $settings['web_title'] = $settings['web_title'] . ' - Tentang';
        return view('tentang', compact('settings', 'logo', 'hero', 'socmed', 'open_time', 'payment', 'pinpoint', 'data'));
    }

    public function tentang_count_view($id) {
        $data = Tentang::find($id);
        $data->view = $data->view + 1;
        $data->save();
    }

    public function tentang_count_facebook($id) {
        $data = Tentang::find($id);
        $data->facebook = $data->facebook + 1;
        $data->save();
    }

    public function tentang_count_twitter($id) {
        $data = Tentang::find($id);
        $data->twitter = $data->twitter + 1;
        $data->save();
    }

    public function tentang_count_whatsapp($id) {
        $data = Tentang::find($id);
        $data->whatsapp = $data->whatsapp + 1;
        $data->save();
    }

    public function login() {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $settings['web_title'] = $settings['web_title'] . ' - Login';
        return view('frontend.member.index', compact('settings', 'logo', 'hero', 'socmed'));
    }

    public function post_login(Request $request) {
        $validator = Validator::make($request->all(), [
            'number_phone' => 'required',
            'password' => 'required'
        ]);
        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $credentials = [
                'number_phone'  => $data['number_phone'],
                'password'  => $data['password'],
                'active' => 1
            ];
            if (Auth::guard('member')->attempt($credentials)) {
                $request->session()->regenerate();
                $response = response()->json(['status' => 'success', 'message' => 'Login Success, Please wait!', 'redirect' => '/']);
            } else {
                $response = response()->json(['status' => 'error', 'message' => 'Username or Password not match! ']);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function logout(Request $request) {
        Auth::guard('member')->logout();
        $request->session()->regenerateToken();
        return redirect()->route('home');
    }

    public function registration() {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $settings['web_title'] = $settings['web_title'] . ' - Login';
        return view('frontend.member.registration', compact('settings', 'logo', 'hero', 'socmed'));
    }

    public function post_registration(Request $request) {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
            'date_birth' => 'required',
            'number_phone' => 'required|unique:members,number_phone',
            'password' => 'required|min:3|max:255'
        ]);

        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $data['password'] = Hash::make($data['password']);
            DB::beginTransaction();
            try {
                Member::create($data);
                DB::commit();
                $response = response()->json(['message' => 'Data has been registered', 'redirect' => 'login']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function profile() {
        foreach (Setting::whereIn('type', ['text', 'textarea'])->get() as $item) {
            $settings[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'logo')->get() as $item) {
            $get[$item['name']]['value'] = $item['value'];
            $get[$item['name']]['description'] = $item['description'];
            $logo = $get;
        }
        foreach (Setting::where('type', 'hero')->get() as $item) {
            $hero[$item['name']] = $item['value'];
        }
        foreach (Setting::where('type', 'socmed')->get() as $item) {
            $socmed[$item['name']] = $item['value'];
        }
        $settings['web_title'] = $settings['web_title'] . ' - Login';
        return view('frontend.member.profile', compact('settings', 'logo', 'hero', 'socmed'));
    }

    public function put_profile(Request $request) {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
            'date_birth' => 'required|max:255'
        ]);

        $data = Member::findOrFail(Auth::guard('member')->user()->id);
        if (!$validator->fails()) {
            $raw = $validator->safe()->all();
            DB::beginTransaction();
            try {
                $data->update($raw);
                DB::commit();
                $response = response()->json(['message' => 'Data has been save', 'redirect' => 'profil']);
            } catch (\Throwable $throw) {
                DB::rollBack();
                Log::error($throw);
                $response = response()->json(['error' => $throw->getMessage()]);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function put_password(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|max:255',
            'password' => 'required|max:255',
        ]);
        $data = Member::findOrFail(Auth::guard('member')->user()->id);
        if (!$validator->fails()) {
            $raw = $validator->safe()->all();
            if (Hash::check($raw['old_password'], $data['password'])) {
                DB::beginTransaction();
                try {
                    $password = Hash::make($raw['password']);
                    $data->update(['password' => $password]);
                    DB::commit();
                    $response = response()->json(['message' => 'Data telah terupdate', 'redirect' => '/']);
                } catch (\Throwable $throw) {
                    DB::rollBack();
                    Log::error($throw);
                    $response = response()->json(['error' => $throw->getMessage()]);
                }
            } else {
                $response = response()->json(['status' => 'error', 'message' => 'Password lama tidak sesuai', 'redirect' => 'profil']);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }
}
