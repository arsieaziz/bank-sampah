<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {
    public function index() {
        $data = Setting::whereIn('name', ['web_title', 'web_description'])->get();
        return view('auth.login', [
            'title' => $data[0]->value . ' - Login App',
            'description' => $data[1]->value,
            'sitename' => $data[0]->value
        ]);
    }

    public function authenticate(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if (!$validator->fails()) {
            $data = $validator->safe()->all();
            $credentials = [
                'username'  => $data['username'],
                'password'  => $data['password'],
                'active' => 1
            ];

            if (Auth::guard('admin')->attempt($credentials)) {
                $request->session()->regenerate();
                $response = response()->json(['status' => 'success', 'message' => 'Login Success, Please wait!', 'redirect' => '/admin']);
            } else {
                $response = response()->json(['status' => 'error', 'message' => 'Username or Password not match! ']);
            }
        } else {
            $response = response()->json(['error' => $validator->errors()]);
        }
        return $response;
    }

    public function logout(Request $request) {
        Auth::guard('admin')->logout();
        // $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('auth');
    }
}
