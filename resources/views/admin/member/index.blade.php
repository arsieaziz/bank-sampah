@extends('admin.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/select2.min.css">
    <link rel="stylesheet" href="css/arsdash/dataTables.bootstrap5.min.css">
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12">
                <h3 class="content-header-title mb-0">Member</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            @foreach ($breadcrumbs as $item)
                                @if (!$item['disabled'])
                                    <li class="breadcrumb-item"><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
                        <h3 class="card-title">Data Member</h3>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end mb-md-0 mb-2">
                        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                            <button class="btn btn-outline-primary" id="refreshTable" type="button"><i class="fas fa-arrows-rotate"></i> Refresh</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="selectStatus">Status<span class="text-danger">*</span></label>
                        <select class="form-select form-control form-control-sm" id="selectStatus" name="status" style="width: 100%;">
                            <option value="" selected>Semua</option>
                            <option value="yes">Aktif</option>
                            <option value="not">Tidak Aktif</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table border-bottom w-100" id="Datatable">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Nama Lengkap</th>
                            <th>Nomor Telepon</th>
                            <th>Member Sejak</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalView" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="formUpdate" method="POST" action="admin/members/update-status">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalViewLabel">Lihat Data Member</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered mb-3">
                            <tr>
                                <td>Nama Lengkap : <br><span class="namaMember fw-bold"></span></td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon / WA : <br><a class="nomorMember fw-bold" target="_blank" style="color:#21a1c2;"></a></td>
                            </tr>
                            <tr>
                                <td>Tanggal Lahir : <br><span class="tgllahirMember fw-bold"></span></td>
                            </tr>
                            <tr>
                                <td>Member Sejak : <br><span class="sejakMember fw-bold"></span></td>
                            </tr>
                            <tr>
                                <td>Status : <br><span class="statusMember"></span></td>
                            </tr>
                        </table>
                        <div id="updateStatusForm">
                            <label for="" class="mb-2">Pilih Update Status :</label>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="statusRadio1" value="0">
                                    <label class="form-check-label" for="statusRadio1">Tidak Aktif</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="statusRadio2" value="1">
                                    <label class="form-check-label" for="statusRadio2">Aktif</label>
                                </div>
                            </div>
                            <input type="hidden" name="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submitStatus">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/select2.min.js"></script>
    <script src="js/arsdash/jquery.dataTables.min.js"></script>
    <script src="js/arsdash/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#selectStatus").select2({
                placeholder: "Please select one",
                allowClear: true,
                width: 'resolve',
            }).on('change', function(e) {
                dataTable.draw();
            });

            $('#refreshTable').click(function() {
                dataTable.draw();
            });

            let dataTable = $('#Datatable').DataTable({
                responsive: true,
                scrollX: false,
                processing: true,
                serverSide: true,
                order: [
                    [3, 'desc']
                ],
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                pageLength: 10,
                ajax: {
                    url: "admin/members",
                    data: function(d) {
                        d.active = $('#selectStatus').find(':selected').val();
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        className: 'text-center',
                        orderable: true,
                        searchable: false
                    },
                    {
                        data: 'fullname',
                        name: 'fullname'
                    },
                    {
                        data: 'number_phone',
                        name: 'number_phone'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        className: 'text-end'
                    },
                    {
                        data: 'active',
                        name: 'active',
                        className: 'text-center',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let status = {
                                0: {
                                    'title': 'Tidak Aktif',
                                    'class': ' bg-danger'
                                },
                                1: {
                                    'title': 'Aktif',
                                    'class': ' bg-success'
                                },
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="badge bg-pill' + status[data].class + '">' +
                                status[data].title +
                                '</span>';
                        },
                    },
                ],
            });

            let modalView = document.getElementById('modalView');
            const bsUpdate = new bootstrap.Modal(modalView);

            modalView.addEventListener('show.bs.modal', function(event) {
                let button = event.relatedTarget
                let id = button.getAttribute('data-id');
                this.querySelector('input[name=id]').value = id;
                $.ajax({
                    type: 'GET',
                    url: 'admin/members/' + id,
                    success: function(response) {
                        $('.namaMember').html(response.fullname);
                        $('.nomorMember').html('<i class="fab fa-whatsapp-square"></i> ' + response.number_phone).attr('href', 'https://wa.me/' + response.number_phone);
                        $('.tgllahirMember').html(response.date_birth);
                        $('.sejakMember').html(response.sejak);
                        $('.statusMember').html(response.status);
                    }
                });
            });

            modalView.addEventListener('hidden.bs.modal', function(event) {
                this.querySelector('input[name=id]').value = '';
            });

            $("#formUpdate").submit(function(e) {
                e.preventDefault();
                let form = $(this);
                let btnSubmit = form.find("[type='submit']");
                let btnSubmitHtml = btnSubmit.html();
                let url = form.attr("action");
                let data = new FormData(this);
                $.ajax({
                    beforeSend: function() {
                        btnSubmit.addClass("disabled").html("<span aria-hidden='true' class='spinner-border spinner-border-sm' role='status'></span> Loading ...").prop("disabled", "disabled");
                    },
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response) {
                        if (!response.error) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            toastr.success(response.message, 'Success !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                            setTimeout(function() {
                                if (response.redirect == "" || response
                                    .redirect == "reload") {
                                    location.reload();
                                } else {
                                    location.href = response.redirect;
                                }
                                dataTable.draw();
                                bsUpdate.hide();
                            }, 1500);
                        } else {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml).prop("disabled", false);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    },
                    error: function(response) {
                        btnSubmit.removeClass("disabled").html(btnSubmitHtml).prop("disabled", false);
                        $.each(response.error, function(i, field) {
                            toastr.error(i + ' ' + field, 'Failed !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                        });
                    }
                });
            });
        });
    </script>
@endsection
