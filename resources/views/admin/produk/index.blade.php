@extends('admin.layouts.main')

@section('importheadAppend')
    <style>
        .active>.page-link,
        .page-link.active {
            background-color: #21a1c2;
            border-color: #21a1c2;
        }
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12">
                <h3 class="content-header-title mb-0">Produk Sampah</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            @foreach ($breadcrumbs as $item)
                                @if (!$item['disabled'])
                                    <li class="breadcrumb-item"><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex align-items-center justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
                        <form action="admin/produk" method="GET">
                            <div class="input-group input-group-sm">
                                <input type="text" name="s" class="form-control form-control-sm" placeholder="Cari..." aria-label="Cari..." style="border-color: #21a1c2;" value="{{ request()->s }}">
                                <button type="submit" class="input-group-text" style="background: #21a1c2;color: #fff;border-color: #21a1c2;"><i class="fa-solid fa-magnifying-glass"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end mb-md-0 mb-2">
                        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                            <a href="admin/produk/create" class="btn btn-sm btn-outline-primary"><i class="fa-solid fa-plus"></i> Tambah Produk Sampah</a>
                        </div>
                    </div>
                </div>
            </div>
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="row mb-4">
                @if (!empty($data) && $data->count())
                    @foreach ($data as $key => $value)
                        <div class="col-lg-4 mb-3">
                            <div class="card w-100" style="box-shadow: none; border-radius: 15px; height: 100%; margin-bottom: 60px; position: relative; padding: 0; overflow: hidden;">
                                <img src="storage/images/thumbnail/{{ $value->main_image }}" class="card-img-top" alt="...">
                                <div class="card-body" style="padding: 10px 20px 20px 20px">
                                    <h5 class="card-title fw-bold mb-0">{{ $value->name }}</h5>
                                    {{-- <span class="catClick" data-slug="{{ $value->categories->slug }}" style="font-size: 14px; color: #21a1c2;"><span class="fw-bold"><i class="fa-solid fa-box-archive"></i></span> {{ $value->categories->title }}</span> --}}
                                    <p class="mb-1 fw-bold">Rp{{ number_format($value->price, 0, '', '.') }}</p>
                                    <p class="mb-1">Stock: {!! $value->stock < 5 ? '<span class="text-danger fw-bold">' . $value->stock . '</span>' : $value->stock !!} Pcs</p>
                                </div>
                                <div class="btn-group" style="position: absolute; bottom: 22px; left: 22px; width: calc(100% - 44px);">
                                    <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#removeProdukModal" data-id="{{ $value->id }}"><i class="fa-solid fa-shop-slash"></i> Hapus</button>
                                    <a href="admin/produk/{{ $value->id }}/edit" class="btn btn-outline-primary"><i class="fa-solid fa-pen-to-square"></i> Edit</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <p class="mb-0">Tidak ada data yang sesuai dengan pencarian anda.</p>
                @endif
            </div>
            {!! $data->appends(Request::all())->links() !!}
            @if ($data->count() && $data->total())
                <p style="font-size: 14px;">Menampilkan {{ $data->count() }} dari total {{ $data->total() }} data</p>
            @endif
        </div>
    </div>

    {{-- Remove Modal --}}
    <div class="modal fade" id="removeProdukModal" aria-labelledby="removeProdukModalLabel" aria-hidden="true">
        <form action="admin/produk" autocomplete="off" method="POST" id="ajax-delete">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="removeProdukModalLabel">Hapus Produk</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        @method('delete')
                        Anda yakin ingin menghapus data ini?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Ya, Hapus</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Remove Modal --}}
@endsection

@section('importfootAppend')
    <script>
        function insertParam(key, value) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);
            var kvp = document.location.search.substr(1).split('&');
            let i = 0;
            for (; i < kvp.length; i++) {
                if (kvp[i].startsWith(key + '=')) {
                    let pair = kvp[i].split('=');
                    pair[1] = value;
                    kvp[i] = pair.join('=');
                    break;
                }
            }
            if (i >= kvp.length) {
                kvp[kvp.length] = [key, value].join('=');
            }
            let params = kvp.join('&');
            document.location.search = params;
        }
        $(document).ready(function() {
            $("#ajax-delete").submit(function(e) {
                e.preventDefault();
                let form = $(this);
                let btnSubmit = form.find("[type='submit']");
                let btnSubmitHtml = btnSubmit.html();
                let url = form.attr("action");
                let data = new FormData(this);
                $.ajax({
                    beforeSend: function() {
                        btnSubmit.addClass("disabled").html(
                            "<span aria-hidden='true' class='spinner-border spinner-border-sm' role='status'></span> Loading ..."
                        ).prop("disabled", "disabled");
                    },
                    cache: false,
                    processData: false,
                    type: "DELETE",
                    url: url,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if (!response.error) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            toastr.success(response.message, 'Success !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                            setTimeout(function() {
                                if (response.redirect == "" || response
                                    .redirect == "reload") {
                                    location.reload();
                                } else {
                                    location.href = response.redirect;
                                }
                            }, 1500);
                        } else {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    },
                    error: function(response) {
                        btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                        $.each(response.error, function(i, field) {
                            toastr.error(i + ' ' + field, 'Failed !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                        });
                    }
                });
            });
            $('.catClick').click(function(e) {
                var slug = $(this).data('slug');
                insertParam('cat', slug);
            });
        });

        const removeProdukModalEl = document.getElementById('removeProdukModal');
        removeProdukModalEl.addEventListener('show.bs.modal', event => {
            let button = event.relatedTarget
            let id = button.getAttribute('data-id');
            var element = document.querySelector('#ajax-delete');
            element.setAttribute('action', 'admin/produk/' + id);
        });
        removeProdukModalEl.addEventListener('hidden.bs.modal', function(event) {
            var element = document.querySelector('#ajax-delete');
            element.setAttribute('action', 'admin/produk');
        });
    </script>
@endsection
