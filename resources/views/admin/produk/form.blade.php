@extends('admin.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/select2.min.css">
    <link rel="stylesheet" href="css/arsdash/dataTables.bootstrap5.min.css">
    <style>
        .select2-container .select2-selection--single {
            height: 31px;
        }

        .body-admin header {
            z-index: 2000;
        }

        .body-admin .side-menu {
            z-index: 1999;
        }

        .note-toolbar {
            background: #f3f3f3;
        }

        .note-modal {
            z-index: 2000 !important;
            margin-top: 100px !important;
        }

        .fade:not(.show) {
            opacity: 1 !important;
        }

        .note-modal-footer {
            height: 55px !important;
            padding: 10px 30px !important;
        }
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-12 col-12">
                <h3 class="content-header-title mb-0">Data Produk Sampah</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            @foreach ($breadcrumbs as $item)
                                @if (!$item['disabled'])
                                    <li class="breadcrumb-item"><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
                        <h3 class="card-title">Form Data Produk Sampah</h3>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end mb-md-0 mb-2">
                        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                            <a class="btn btn-outline-primary" href="admin/produk"><i class="fas fa-arrow-left"></i> Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button class="nav-link active" id="form-tab" data-bs-toggle="tab" data-bs-target="#form" type="button" role="tab" aria-controls="form" aria-selected="true">Form</button>
                    @if ($mode == 'edit')
                        <button class="nav-link" id="image-upload-tab" data-bs-toggle="tab" data-bs-target="#image-upload" type="button" role="tab" aria-controls="image-upload" aria-selected="false">Upload Image</button>
                    @endif
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="form" role="tabpanel" aria-labelledby="form-tab" tabindex="0">
                    <form action="admin/produk{{ isset($data['name']) ? '/' . request()->segment(3) : null }}" autocomplete="off" method="POST" class="ajax">
                        @if (isset($data['name']))
                            @method('PUT')
                        @endif
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group mb-3">
                                        <label class="mb-1">Nama Produk Sampah</label>
                                        <input type="text" class="form-control form-control-sm" name="name" required="required" value="{{ isset($data['name']) ? $data['name'] : null }}" placeholder="Isi nama produk anda">
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group mb-3">
                                                <label class="mb-1">Harga Produk Sampah</label>
                                                <div class="input-group input-group-sm">
                                                    <span class="input-group-text">Rp</span>
                                                    <input type="text" class="form-control form-control-sm" name="price" required="required" value="{{ isset($data['price']) ? $data['price'] : null }}" placeholder="Isi harga produk sampah anda">
                                                    <span class="input-group-text">/ Kg</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label class="mb-1">Deskripsi Produk Sampah</label>
                                        <textarea id="summernote" name="description">{{ isset($data['description']) ? $data['description'] : null }}</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group mb-3">
                                        <label class="d-block mb-1">Status Produk Sampah</label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="active" id="inlineRadio1" value="1" {{ isset($data['active']) && $data['active'] == 1 ? 'checked' : null }} {{ !isset($data['active']) ? 'checked' : null }}>
                                            <label class="form-check-label" for="inlineRadio1">Aktif</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="active" id="inlineRadio2" value="0" {{ isset($data['active']) && $data['active'] == 0 ? 'checked' : null }}>
                                            <label class="form-check-label" for="inlineRadio2">Tidak Aktif</label>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label class="d-block mb-1">Photo Utama</label>
                                        <input type="file" name="main_image" accept=".jpg, .jpeg, .png" class="form-control form-control-sm mb-1 form-image" />
                                        <img class="img-fluid d-block mb-1" src="{{ isset($data['main_image']) ? 'storage/images/thumbnail/' . $data['main_image'] : 'images/no-image-s.jpg' }}" />
                                        <span class="text-center" style="font-size: 12px; line-height: 12px; display: block;">Recomended dimention 500x500 pixel and max filesize 2.0MB</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer" style="border: none; background: transparent;">
                            <div class="btn-group" role="group">
                                <button type="submit" class="btn btn-sm btn-outline-primary text-white">{!! isset($data['id']) ? 'Save <i class="fa fa-fw fa-save"></i>' : 'Next <i class="fa-solid fa-arrow-right"></i>' !!}</button>
                            </div>
                        </div>
                    </form>
                </div>
                @if ($mode == 'edit')
                    <div class="tab-pane fade" id="image-upload" role="tabpanel" aria-labelledby="image-upload-tab" tabindex="0">
                        <div class="table-responsive p-3">
                            <table class="table border-bottom w-100" id="DatatablePhoto">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <form action="{{ route('produk.uploadimage') }}" class="dropzone" id="uploadimage">
                            @csrf
                            <input type="hidden" name="parent_id" value="{{ $data['id'] }}">
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDeleteLabel">Delete Confirm</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formDelete" action="{{ route('produk.deleteimage') }}">
                    <div class="modal-body">
                        @csrf
                        @method('POST')
                        <input type="hidden" name="iddelete">
                        Anda yakin ingin menghapus data ini?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/select2.min.js"></script>
    <script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
    @if ($mode == 'edit')
        <link rel="stylesheet" href="css/arsdash/dropzone.css">
        <script src="js/arsdash/dropzone.js"></script>
        <script src="js/arsdash/jquery.dataTables.min.js"></script>
        <script src="js/arsdash/dataTables.bootstrap5.min.js"></script>
    @endif
    <script>
        @if ($mode == 'edit')
            Dropzone.options.uploadimage = {
                paramName: 'images',
                maxFilesize: 5,
                acceptedFiles: 'image/*',
                dictDefaultMessage: "<i class='fa fa-upload fa-4x'></i><h3>Upload file disini <small>(click or drop files)</small></h3>",
                success(file, response) {
                    toastr.success(response.message, 'Success !', {
                        progressBar: true,
                        timeOut: 1000
                    });
                },
                error(file, response) {
                    toastr.error(response, 'Failed !', {
                        progressBar: true,
                        timeOut: 1000
                    });
                },
                queuecomplete() {
                    setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            }
        @endif
        $(document).ready(function() {
            $('input[name="price"]').mask('000.000.000', {
                reverse: true
            });
            $('#summernote').summernote({
                placeholder: 'Isi deskripsi produk anda disini',
                height: 150,
                focus: true,
                dialogsInBody: true,
                dialogsFade: true,
                lang: 'id-ID',
                tabsize: 2,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });
            $(".form-image").change(function() {
                var thumb = $(this).parent('.form-group').find('img');
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        thumb.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
            @if ($mode == 'edit')
                let dataTable = $('#DatatablePhoto').DataTable({
                    responsive: true,
                    scrollX: false,
                    processing: true,
                    serverSide: true,
                    order: [
                        [1, 'asc']
                    ],
                    lengthMenu: [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ],
                    pageLength: 10,
                    ajax: {
                        url: "{{ route('produk.getimage') }}",
                        data: {
                            id: "{{ isset($data['id']) ? $data['id'] : null }}"
                        }
                    },
                    columns: [{
                            data: 'value',
                            name: 'value',
                            render: function(data, type, full, meta) {
                                return '<img src="storage/images/thumbnail/' + data + '" style="width: 150px;">';
                            },
                        },
                        {
                            data: 'hapus',
                            name: 'hapus',
                            className: 'text-center align-middle',
                            orderable: false,
                            searchable: false
                        }
                    ],
                });

                let modalDelete = document.getElementById('modalDelete');
                const bsDelete = new bootstrap.Modal(modalDelete);

                modalDelete.addEventListener('show.bs.modal', function(event) {
                    let button = event.relatedTarget
                    let id = button.getAttribute('data-bs-id');
                    var element = document.querySelector('#formDelete');
                    document.querySelector('[name="iddelete"]').value = id;
                    element.setAttribute('action', '{{ route('produk.deleteimage') }}');
                });

                modalDelete.addEventListener('hidden.bs.modal', function(event) {
                    var element = document.querySelector('#formDelete');
                    document.querySelector('[name="iddelete"]').value = '';
                    element.setAttribute('action', '{{ route('produk.deleteimage') }}');
                });

                $("#formDelete").submit(function(e) {
                    e.preventDefault();
                    let form = $(this);
                    let btnSubmit = form.find("[type='submit']");
                    let btnSubmitHtml = btnSubmit.html();
                    let url = form.attr("action");
                    let data = new FormData(this);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<span aria-hidden='true' class='spinner-border spinner-border-sm' role='status'></span> Loading ..."
                            ).prop("disabled", "disabled");
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: "post",
                        url: url,
                        data: data,
                        headers: {
                            'X-CSRF-TOKEN': $('input[name="_token"]').val()
                        },
                        success: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr(
                                "disabled");
                            if (response.status === "success") {
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                dataTable.draw();
                                bsDelete.hide();
                            } else {
                                toastr.error((response.message ? response.message :
                                    "Please complete your form"), 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                dataTable.draw();
                                bsDelete.hide();
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml).removeAttr(
                                "disabled");
                            toastr.error(response.responseJSON.message, 'Failed !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                            dataTable.draw();
                            bsDelete.hide();
                        }
                    });
                });
            @endif
        });
    </script>
@endsection
