@extends('admin.layouts.main')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12">
                <h3 class="content-header-title mb-0">Roles</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            @foreach ($breadcrumbs as $item)
                                @if (!$item['disabled'])
                                    <li class="breadcrumb-item"><a href="{{ $item['url'] }}">{{ $item['title'] }}</a>
                                    </li>
                                @else
                                    <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
                        <h3 class="card-title">Form Roles</h3>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end mb-md-0 mb-2">
                        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                            <a class="btn btn-outline-primary" href="javascript:history.back();"><i class="fas fa-arrow-left"></i>
                                Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <form action="admin/roles{{ isset($data['name']) ? '/' . request()->segment(3) : null }}" autocomplete="off" method="POST" class="ajax">
                @if (isset($data['name']))
                    @method('PUT')
                @endif
                @csrf
                <div class="card-body">
                    <div class="form-group mb-3">
                        <label>Name</label>
                        <input type="text" class="form-control form-control-sm" name="name" required="required" value="{{ isset($data['name']) ? $data['name'] : null }}" placeholder="Type name here" />
                    </div>
                    <ul class="hak-akses" style="padding: 0;list-style: none;">
                        <li><label><input type="checkbox" class="master-check"> Web Settings<sub><small style="font-weight:400;"><i>Semua</i></small><sub></label>
                            <ul>
                                <li><input type="checkbox" name="akses[]" value="users" {{ !empty($data['access']) && in_array('users', $data['access']) ? 'checked' : null }}> Users</li>
                                <li><input type="checkbox" name="akses[]" value="roles" {{ !empty($data['access']) && in_array('roles', $data['access']) ? 'checked' : null }}> Roles</li>
                                <li><input type="checkbox" name="akses[]" value="members" {{ !empty($data['access']) && in_array('members', $data['access']) ? 'checked' : null }}> Members</li>
                                <li><input type="checkbox" name="akses[]" value="settings" {{ !empty($data['access']) && in_array('settings', $data['access']) ? 'checked' : null }}> Settings</li>
                                <li><input type="checkbox" name="akses[]" value="tentang" {{ !empty($data['access']) && in_array('tentang', $data['access']) ? 'checked' : null }}> Tentang</li>
                                <li><input type="checkbox" name="akses[]" value="produk" {{ !empty($data['access']) && in_array('produk', $data['access']) ? 'checked' : null }}> Produk</li>
                                <li><input type="checkbox" name="akses[]" value="penjualan" {{ !empty($data['access']) && in_array('penjualan', $data['access']) ? 'checked' : null }}> Penjualan</li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="card-footer" style="border: none; background: transparent;">
                    <div class="btn-group" role="group">
                        <button type="submit" class="btn btn-sm btn-outline-primary text-white">Save <i class="fa fa-fw fa-save"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('importfootAppend')
    <script>
        $(document).ready(function() {
            $(".master-check").change(function() {
                if (this.checked) {
                    $(this).parents("label").next().find("input:checkbox").each(function() {
                        this.checked = true;
                    });
                } else {
                    $(this).parents("label").next().find("input:checkbox").each(function() {
                        this.checked = false;
                    });
                }
            });

            $("input:checkbox").click(function() {
                var buyut = $(this).parentsUntil(".hak-akses").children("li").children(
                    "input.master-check");
                var master_check = $(this).parent().parent().parent().children("label").children(
                    ".master-check");
                var sodara = $(this).parent().parent().find("li :checkbox");

                if ($(sodara).filter(':not(:checked)').length > 0) {
                    $(master_check).prop("checked", false);
                    if ($(this).parentsUntil(".hak-akses").find(":checkbox").filter(':not(:checked)')
                        .length > 0) {
                        $(buyut).prop("checked", false);
                    } else {
                        $(buyut).prop("checked", true);
                    }
                } else {
                    $(master_check).prop("checked", true);
                }
            });
        });
    </script>
@endsection
