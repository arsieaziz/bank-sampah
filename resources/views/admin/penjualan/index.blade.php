@extends('admin.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/select2.min.css">
    <link rel="stylesheet" href="css/arsdash/dataTables.bootstrap5.min.css">
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12">
                <h3 class="content-header-title mb-0">Penjualan</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            @foreach ($breadcrumbs as $item)
                                @if (!$item['disabled'])
                                    <li class="breadcrumb-item"><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start align-items-center mb-3 mb-md-0">
                        <h3 class="card-title">Data Penjualan</h3>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end mb-md-0 mb-2">
                        <div class="btn-group float-md-right" role="group" aria-label="Button group with nested dropdown">
                            <button class="btn btn-outline-primary" id="refreshTable" type="button"><i class="fas fa-arrows-rotate"></i> Refresh</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="selectStatus">Status Penjualan <span class="text-danger">*</span></label>
                        <select class="form-select form-control form-control-sm" id="selectStatus" name="status" style="width: 100%;">
                            <option value="" selected>Semua</option>
                            <option value="booked">Dipesan</option>
                            <option value="cancel">Dibatalkan</option>
                            <option value="done">Selesai</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table border-bottom w-100" id="Datatable">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Nama Pembeli</th>
                            <th>Total Bayar</th>
                            <th>Waktu Pesan</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalUpdate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalUpdateLabel">Update Status Penjualan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="formUpdate" method="POST" action="admin/penjualan/update-status">
                    <div class="modal-body">
                        @csrf
                        <table class="table table-bordered mb-0">
                            <tr>
                                <td>Nama Pemesan : <br><span class="namaPemesan fw-bold"></span></td>
                            </tr>
                            <tr>
                                <td>Nomor Whatsapp : <br><a class="nomorPemesan fw-bold" target="_blank" style="color:#21a1c2;"></a></td>
                            </tr>
                            <tr>
                                <td>Produk : <br><span class="productList"></span></td>
                            </tr>
                        </table>
                        <table class="table table-bordered" id="detailPesanan">
                            <tbody></tbody>
                        </table>
                        <div id="updateStatusForm">
                            <label for="" class="mb-2">Pilih Update Status :</label>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="statusRadio1" value="cancel">
                                    <label class="form-check-label" for="statusRadio1">Dibatalkan</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="statusRadio2" value="done">
                                    <label class="form-check-label" for="statusRadio2">Selesaikan Pembayaran</label>
                                </div>
                            </div>
                            <small class="text-danger" style="font-size: 11px;">*Harap teliti untuk memilih update status</small>
                            <input type="hidden" name="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submitStatus">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/select2.min.js"></script>
    <script src="js/arsdash/jquery.dataTables.min.js"></script>
    <script src="js/arsdash/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#selectStatus").select2({
                placeholder: "Please select one",
                allowClear: true,
                width: 'resolve',
            }).on('change', function(e) {
                dataTable.draw();
            });

            $('#refreshTable').click(function() {
                dataTable.draw();
            });

            let dataTable = $('#Datatable').DataTable({
                responsive: true,
                scrollX: false,
                processing: true,
                serverSide: true,
                order: [
                    [3, 'desc']
                ],
                lengthMenu: [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                pageLength: 10,
                ajax: {
                    url: "admin/penjualan",
                    data: function(d) {
                        d.status = $('#selectStatus').find(':selected').val();
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        className: 'text-center',
                        orderable: true,
                        searchable: false
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'total_pay',
                        name: 'total_pay',
                        className: 'text-end'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                        className: 'text-end'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        className: 'text-center',
                        orderable: true,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let status = {
                                'booked': {
                                    'title': 'Dipesan',
                                    'class': ' bg-danger'
                                },
                                'cancel': {
                                    'title': 'Dibatalkan',
                                    'class': ' bg-secondary'
                                },
                                'done': {
                                    'title': 'Selesai',
                                    'class': ' bg-success'
                                }
                            };
                            if (typeof status[data] === 'undefined') {
                                return data;
                            }
                            return '<span class="badge bg-pill' + status[data].class + '">' + status[data].title + '</span>';
                        },
                    },
                ],
            });

            function addCommas(nStr) {
                nStr += '';
                x = nStr.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + '.' + '$2');
                }
                return x1 + x2;
            }

            let modalUpdate = document.getElementById('modalUpdate');
            const bsUpdate = new bootstrap.Modal(modalUpdate);

            modalUpdate.addEventListener('show.bs.modal', function(event) {
                let button = event.relatedTarget
                let id = button.getAttribute('data-id');
                this.querySelector('input[name=id]').value = id;
                $.ajax({
                    type: 'GET',
                    url: 'admin/penjualan/' + id,
                    success: function(response) {
                        $('.namaPemesan').html(response.membership.fullname.replace(/\%20/g, ' '));
                        $('.nomorPemesan').html('<i class="fab fa-whatsapp-square"></i> ' + response.membership.number_phone).attr('href', 'https://wa.me/' + response.membership.number_phone);
                        var product = '<ul style="padding-left:15px; margin-bottom:0;">';
                        var jmlkg = 0;
                        var getProduct = JSON.parse(response.product);
                        getProduct.forEach(element => {
                            jmlkg = (jmlkg + parseFloat(element.qty));
                            product += '<li style="border-bottom: 1px solid #eaeaea;"> Nama : <span class="fw-bold">' + element.title + '</span><br> Harga : <span class="fw-bold">Rp ' + addCommas(element.price) + ',-</span><br> Jumlah : <span class="fw-bold">' + element.qty + '/Kg</span><br> Subtotal : <span class="fw-bold">Rp ' + addCommas(element.price * element.qty) + ',-</span>';
                        });
                        product += '</ul>';
                        $('.productList').html(product);
                        $('#detailPesanan').append('<tr><td>Total Harga (' + response.jumlah_produk + ' Produk / ' + jmlkg + ' Kg) : <br><b>Rp ' + addCommas(response.total_harga) + '</b></td></tr>');
                        $('#detailPesanan').append('<tr><td>Total Perlu Bayar : <br><b>Rp ' + addCommas(response.total_harga) + '</b></td></tr>');
                        if (response.status != 'booked') {
                            $('#updateStatusForm').addClass('d-none');
                            $('#submitStatus').addClass('d-none');
                            if (response.status == 'cancel') {
                                response.status = '<b class="text-secondary">Dibatalkan</b>';
                            } else if (response.status == 'done') {
                                response.status = '<b class="text-success">Selesai Pembayaran</b>';
                            }
                            $('#detailPesanan').append('<tr><td>Status : <br>' + response.status + '</td></tr>');
                        }
                    }
                });
            });

            modalUpdate.addEventListener('hidden.bs.modal', function(event) {
                $('#updateStatusForm').removeClass('d-none');
                $('#submitStatus').removeClass('d-none');
                this.querySelector('input[name=id]').value = '';
                $('#detailPesanan').html('<tbody></tbody>');
            });

            $("#formUpdate").submit(function(e) {
                e.preventDefault();
                let form = $(this);
                let btnSubmit = form.find("[type='submit']");
                let btnSubmitHtml = btnSubmit.html();
                let url = form.attr("action");
                let data = new FormData(this);
                $.ajax({
                    beforeSend: function() {
                        btnSubmit.addClass("disabled").html("<span aria-hidden='true' class='spinner-border spinner-border-sm' role='status'></span> Loading ...").prop("disabled", "disabled");
                    },
                    cache: false,
                    processData: false,
                    contentType: false,
                    type: "POST",
                    url: url,
                    data: data,
                    success: function(response) {
                        if (!response.error) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            toastr.success(response.message, 'Success !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                            setTimeout(function() {
                                if (response.redirect == "" || response
                                    .redirect == "reload") {
                                    location.reload();
                                } else {
                                    location.href = response.redirect;
                                }
                                dataTable.draw();
                                bsUpdate.hide();
                            }, 1500);
                        } else {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml).prop("disabled", false);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    },
                    error: function(response) {
                        btnSubmit.removeClass("disabled").html(btnSubmitHtml).prop("disabled", false);
                        $.each(response.error, function(i, field) {
                            toastr.error(i + ' ' + field, 'Failed !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                        });
                    }
                });
            });
        });
    </script>
@endsection
