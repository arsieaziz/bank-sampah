@extends('admin.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/flatpickr.min.css">
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12">
                <h3 class="content-header-title mb-0">Dashboard</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/admin">Dashboard</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <p class="mb-5">Selamat datang kembali {{ auth()->user()->name }}</p>
            <div class="row">
                <div class="col-sm-6">
                    <table class="table align-middle mb-5">
                        <tr>
                            <td style="width: 60%">Pesanan Selesai Bayar</td>
                            <td style="width: 3px">:</td>
                            <td class="text-end"><b>{{ number_format($saleToday['jumlah_pesanan'], 0, ',', '.') }} Order</b></td>
                        </tr>
                        <tr>
                            <td style="width: 60%">Jumlah Sampah Hari Ini</td>
                            <td style="width: 3px">:</td>
                            <td class="text-end"><b>{{ $saleToday['jumlah_produk'] }}/Kg</b></td>
                        </tr>
                        <tr>
                            <td style="width: 60%">Penjualan Hari Ini</td>
                            <td style="width: 3px">:</td>
                            <td class="text-end"><b>Rp {{ number_format($saleToday['jumlah_penjualan'], 0, ',', '.') }}</b></td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table class="table align-middle mb-5">
                        <tr>
                            <td style="width: 60%">Jumlah Member Aktif</td>
                            <td style="width: 3px">:</td>
                            <td class="text-end"><b>{{ number_format($count['member'], 0, ',', '.') }} Orang</b></td>
                        </tr>
                        <tr>
                            <td style="width: 60%">Jumlah Produk Aktif</td>
                            <td style="width: 3px">:</td>
                            <td class="text-end"><b>{{ number_format($count['produk'], 0, ',', '.') }} Pcs</b></td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6">
                    <p class="mb-0">10 (Sepuluh) Member terbanyak pesan :</p>
                    <table class="table align-middle mb-5">
                        @if (!empty($memberSale))
                            @foreach ($memberSale as $key => $item)
                                <tr>
                                    <td class="fw-bold" style="width: 15px; padding: 0;">{{ $key + 1 }}.</td>
                                    <td>
                                        <b class="d-block">{{ $item->fullname }}</b>
                                        <span style="font-size: 12px; font-style: italic; display: block;">{{ date('d F Y - H:i:s', strtotime($item->created_at)) }}</span>
                                        <a href="https://wa.me/{{ '62' . ltrim($item->number_phone, '0') }}" target="_blank" style="font-size: 12px; font-style: italic; display: block; color: #139ac9;"><i class="fab fa-whatsapp-square"></i> {{ $item->number_phone }}</a>
                                    </td>
                                    <td class="text-end"><b>{{ number_format($item->total, 0, ',', '.') }} Order</b></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                <div class="col-sm-6">
                    <p class="mb-0">10 (Sepuluh) Produk terbanyak terjual :</p>
                    <table class="table align-middle mb-5">
                        @if (!empty($product['sale']))
                            @foreach ($product['sale'] as $key => $item)
                                <tr>
                                    <td class="fw-bold" style="width: 15px; padding: 0;">{{ $key + 1 }}.</td>
                                    <td>
                                        <a href="katalog/{{ $item->slug }}" target="_blank" class="d-block fw-bold">{{ $item->name }}</a>
                                        <span style="font-size: 12px; font-style: italic; display: block;">Harga : Rp {{ number_format($item->price, '0', ',', '.') }}/Kg</span>
                                    </td>
                                    <td class="text-end"><b>{{ $item->sale }}/Kg</b></td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
            <div id="container" style="width: 100%;">
                <canvas id="bar-chart" width="100%" height="50px"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('importfootAppend')
    <script type="text/javascript" src="js/arsdash/chart.umd.js"></script>
    <script>
        $(document).ready(function() {
            new Chart(document.getElementById("bar-chart"), {
                type: 'bar',
                data: {
                    labels: {!! json_encode($product['chart_name']) !!},
                    datasets: [{
                        label: "Jumlah Sampah",
                        data: {!! json_encode($product['chart_sale']) !!}
                    }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Predicted world population (millions) in 2050'
                    }
                }
            });
        });
    </script>
@endsection
