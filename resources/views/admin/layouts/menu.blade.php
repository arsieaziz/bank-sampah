@php
    $role_id = auth()->user()->role_id;
    $access = [];
    if (!empty(auth()->user()->roles->access)) {
        $access = json_decode(auth()->user()->roles->access, true);
    }
@endphp
<div class="side-menu">
    <ul>
        <li class="list-title">App Settings</li>
        <li class="{{ request()->segment(2) == '' ? 'active' : null }}"><a href="admin"><i class="fa fa-home"></i> Dashboard</a></li>
        <li class="{{ request()->segment(2) == 'settings' ? 'active' : null }} {{ (!empty($access) && in_array('settings', $access)) || $role_id == 0 ? null : 'd-none' }}"><a href="admin/settings"><i class="fa fa-cogs"></i> Pengaturan</a></li>
        <li class="{{ request()->segment(2) == 'users' ? 'active' : null }} {{ (!empty($access) && in_array('users', $access)) || $role_id == 0 ? null : 'd-none' }}"><a href="admin/users"><i class="fa fa-user"></i> Users</a></li>
        <li class="{{ request()->segment(2) == 'members' ? 'active' : null }} {{ (!empty($access) && in_array('members', $access)) || $role_id == 0 ? null : 'd-none' }}"><a href="admin/members"><i class="fa fa-users"></i> Members</a></li>
        <li class="{{ request()->segment(2) == 'tentang' ? 'active' : null }} {{ (!empty($access) && in_array('tentang', $access)) || $role_id == 0 ? null : 'd-none' }}"><a href="admin/tentang"><i class="fa-solid fa-landmark"></i> Tentang</a></li>
        <li class="{{ request()->segment(2) == 'produk' ? 'active' : null }} {{ (!empty($access) && in_array('produk', $access)) || $role_id == 0 ? null : 'd-none' }}"><a href="admin/produk"><i class="fa-solid fa-boxes-packing"></i> Produk Sampah</a></li>
        <li class="{{ request()->segment(2) == 'penjualan' ? 'active' : null }} {{ (!empty($access) && in_array('penjualan', $access)) || $role_id == 0 ? null : 'd-none' }}"><a href="admin/penjualan"><i class="fas fa-coins"></i> Penjualan</a></li>
    </ul>
</div>
