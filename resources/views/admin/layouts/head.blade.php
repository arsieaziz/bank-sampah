<header>
    <div class="main-header">
        <div class="left">
            <div class="mobile-icon">
                <div class="menu-opener">
                    <div class="menu-opener-inner"></div>
                </div>
            </div>
            <div class="logo">
                <img src="images/logo.png" alt="Logo">
                <h1>Bank Sampah</h1>
            </div>
            <button class="mobile-icon-right">
                <i class="fas fa-ellipsis-v"></i>
            </button>
        </div>
        <div class="right">
            <button class="btn btn-hide-aside" id="btn-hide-aside"><i class="fas fa-align-left"></i></button>
            <div class="menu">
                <ul>
                    <li><a href="admin/penjualan" class="fw-bold"><i class="fas fa-bell"></i> Pesanan <span id="notifPesanan" class="badge text-bg-success">0</span></a></li>
                    <li>
                        <a href="#" class="dropdown-toggle dropdown-menu-right" type="button" data-bs-toggle="dropdown">
                            <div class="avatar" style="border-radius: 10px;"><img src="{{ !empty(auth()->user()->image) ? 'storage/images/thumbnail/' . auth()->user()->image : 'images/man.png' }}" alt=""></div>
                            <div class="name fw-bold">{{ auth()->user()->username }}</div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item" href="admin/profile"><i class="fas fa-user"></i> Profile</a>
                            <a class="dropdown-item" href="javascript:void(0);" onclick="window.document.getElementById('formLogout').submit();"><i class="fas fa-sign-out-alt"></i> Log Out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
<form id="formLogout" class="d-none" action="/logout" method="POST">
    @csrf @method('POST')
</form>
