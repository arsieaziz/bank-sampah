@extends('admin.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/bootstrap-editable.css">
    <link rel="stylesheet" href="css/arsdash/select2.min.css">
    <style>
        .input-hidden {
            /* For Hiding Radio Button Circles */
            position: absolute;
            left: -9999px;
        }

        input[type="radio"]:checked+label>img {
            border: 2px solid #21a1c2;
            font-weight: bold;
            color: #21a1c2;
        }

        input[type="radio"]:checked+label>span {
            font-weight: bold;
            color: #21a1c2;
        }

        input[type="radio"]+label>img {
            border: 2px solid transparent;
            transition: 500ms all;
        }

        .form-check-inline {
            margin: 10px;
        }

        .form-check-label span {
            display: block;
            text-align: center;
        }

        .form-check {
            padding: 0;
        }
    </style>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12">
                <h3 class="content-header-title mb-0">Pengaturan</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            @foreach ($breadcrumbs as $item)
                                @if (!$item['disabled'])
                                    <li class="breadcrumb-item"><a href="{{ $item['url'] }}">{{ $item['title'] }}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{ $item['title'] }}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mb-2">
            <div class="d-flex justify-content-center flex-wrap">
                <button type="button" class="btn btn-outline-primary me-2 mb-2" data-bs-toggle="modal" data-bs-target="#logoModal"><i class="fas fa-image"></i> Logo</button>
                <button type="button" class="btn btn-outline-primary me-2 mb-2" data-bs-toggle="modal" data-bs-target="#heroModal"><i class="fas fa-images"></i> Hero</button>
                <button type="button" class="btn btn-outline-primary me-2 mb-2" data-bs-toggle="modal" data-bs-target="#timeModal"><i class="fa-solid fa-clock-rotate-left"></i> Jam Buka</button>
                <button type="button" class="btn btn-outline-primary me-2 mb-2" data-bs-toggle="modal" data-bs-target="#pinPointModal"><i class="fa-solid fa-location-dot"></i> Titik Lokasi</button>
                <button type="button" class="btn btn-outline-primary me-2 mb-2" data-bs-toggle="modal" data-bs-target="#socmedModal"><i class="fa-brands fa-meta"></i> Social Media</button>
                <button type="button" class="btn btn-outline-primary me-2 mb-2" data-bs-toggle="modal" data-bs-target="#paymentMethodModal"><i class="fa-solid fa-money-bill-wave"></i> Metode Pembayaran</button>
            </div>
        </div>
        <div class="card">
            <div class="table-responsive">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <table class="table border-bottom w-100" id="Datatable">
                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td style="width:180px;">{{ $item['description'] }}</td>
                                <td style="width:10px;">:</td>
                                <td style="width:calc(100% - 190px);">
                                    <a href="#" class="editable" e-style="width: 100%" data-name="keyword" data-type="{{ $item['type'] }}" data-pk="{{ $item['id'] }}" data-url="/admin/settings">{{ $item['value'] }}</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- Logo Modal --}}
    <div class="modal fade" id="logoModal" tabindex="-1" aria-labelledby="logoModalLabel" aria-hidden="true">
        <form action="admin/settings/update-logo" autocomplete="off" method="POST" id="ajax-logo">
            @method('PUT')
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="logoModalLabel">Pengaturan Logo</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-4">
                            <div class="row">
                                <div class="col-sm-8 form-group">
                                    <label class="d-block">Logo Image</label>
                                    <input type="file" name="image" accept=".jpg, .jpeg, .png" class="form-control form-control-sm mb-1 form-image" />
                                    <img class="d-block mb-1" style="height: 60px;" src="{{ isset($logo['image']['value']) ? 'storage/images/original/' . $logo['image']['value'] : 'images/logo.png' }}" />
                                    <small class="text-center">Recomended max filesize 2.0MB</small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <label>Logo Text Pertama</label>
                            <div class="input-group">
                                <input type="text" class="form-control form-control-sm" name="text_first" value="{{ isset($logo['text_first']['value']) ? $logo['text_first']['value'] : null }}" placeholder="Tulis Teks Logo Pertama" />
                                <span class="input-group-text"><input type="color" name="color_first" value="{{ isset($logo['text_first']['description']) ? $logo['text_first']['description'] : null }}"></span>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <label>Logo Text Kedua</label>
                            <div class="input-group">
                                <input type="text" class="form-control form-control-sm" name="text_second" value="{{ isset($logo['text_second']['value']) ? $logo['text_second']['value'] : null }}" placeholder="Tulis Teks Logo Kedua" />
                                <span class="input-group-text"><input type="color" name="color_second" value="{{ isset($logo['text_second']['description']) ? $logo['text_second']['description'] : null }}"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Logo Modal --}}

    {{-- Hero Modal --}}
    <div class="modal fade" id="heroModal" tabindex="-1" aria-labelledby="heroModalLabel" aria-hidden="true">
        <form action="admin/settings/update-hero" autocomplete="off" method="POST" id="ajax-hero">
            @method('PUT')
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="heroModalLabel">Pengaturan Hero</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="mb-4">
                            <div class="row">
                                <div class="col-sm-8 form-group">
                                    <label class="d-block">Hero Image</label>
                                    <input type="file" name="image" accept=".jpg, .jpeg, .png" class="form-control form-control-sm mb-1 form-image" />
                                    <img class="d-block mb-1" style="width: 80%" src="{{ isset($hero['image']) ? 'storage/images/thumbnail/' . $hero['image'] : 'images/no-image.jpg' }}" />
                                    <small class="text-center">Recomended max filesize 2.0MB</small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <label>Judul</label>
                            <input type="text" class="form-control form-control-sm" name="title" value="{{ isset($hero['title']) ? $hero['title'] : null }}" placeholder="Tulis Judul Hero" />
                        </div>
                        <div class="form-group mb-4">
                            <label>Deskripsi</label>
                            <textarea class="form-control form-control-sm" name="description" rows="3" placeholder="Tulis Deskripsi Hero">{{ isset($hero['description']) ? $hero['description'] : null }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Hero Modal --}}

    {{-- Time Modal --}}
    <div class="modal fade" id="timeModal" tabindex="-1" aria-labelledby="timeModalLabel" aria-hidden="true">
        <form action="admin/settings/update-time" autocomplete="off" method="POST" id="ajax-time">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="timeModalLabel">Pengaturan Jam Buka</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-dark table-hover">
                            <thead>
                                <tr>
                                    <th><i class="fa-solid fa-arrow-down-1-9"></i></th>
                                    <th>Hari</th>
                                    <th>Jam</th>
                                    <th class="text-center">Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($time->isNotEmpty())
                                    @foreach ($time as $i => $val)
                                        @php
                                            $i++;
                                        @endphp
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $val['name'] }}</td>
                                            <td>{{ $val['description'] }}</td>
                                            <td class="text-center"><a href="" class="delete-time" data-type="open_time" data-pk="{{ $val['id'] }}" data-url="admin/settings/delete-time"><i class="fa-regular fa-trash-can"></i></a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Data Kosong</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="form-group mb-4">
                            <label>Hari</label>
                            <input type="text" class="form-control form-control-sm" name="name" value="{{ isset($time['name']) ? $time['name'] : null }}" placeholder="Tulis Hari Buka (cth: Senin - Jumat)" />
                        </div>
                        <div class="form-group mb-4">
                            <label>Jam</label>
                            <input type="text" class="form-control form-control-sm" name="description" value="{{ isset($time['description']) ? $time['description'] : null }}" placeholder="Tulis Jam Buka (cth: 08.00 - 17.00 WIB)" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Time Modal --}}

    {{-- Pinpoint Modal --}}
    <div class="modal fade" id="pinPointModal" aria-labelledby="pinPointModalLabel" aria-hidden="true">
        <form action="admin/settings/update-pinpoint" autocomplete="off" method="POST" id="ajax-pinpoint">
            @method('PUT')
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="pinPointModalLabel">Pengaturan Titik Lokasi</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p style="font-size: 13px">Silahkan Geser Point Berwarna Merah atau cari berdasarkan "wilayah" dan Sesuaikan dengan alamat Anda, Atau Pastikan GPS Perangkat Anda Hidup jika anda menggunakan layanan "Ambil Koordinat posisi sekarang" dengan klik tombol di bawah ini:</p>
                        <button type="button" id="getLocateNow" class="btn btn-sm" style="border: 1px solid rgba(0, 0, 0, 0.5); margin-bottom: 16px;"><i class="fa-solid fa-location-crosshairs text-danger"></i> Ambil Koordinat Posisi Sekarang</button>
                        <div id="mapid"></div>
                        <input id="latlng" type="hidden" name="pinpoint" value="{{ isset($pinpoint) ? $pinpoint->first() : null }}">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Pinpoint Modal --}}

    {{-- Socmed Modal --}}
    <div class="modal fade" id="socmedModal" tabindex="-1" aria-labelledby="socmedModalLabel" aria-hidden="true">
        <form action="admin/settings/update-socmed" autocomplete="off" method="POST" id="ajax-socmed">
            @method('PUT')
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="socmedModalLabel">Pengaturan Social Media</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group mb-3">
                            <label>Whatsapp</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa-brands fa-whatsapp"></i></span>
                                <input type="text" class="form-control form-control-sm" pattern="^(0)8[1-9][0-9]{6,9}$" name="whatsapp" value="{{ isset($socmed['whatsapp']) ? $socmed['whatsapp'] : null }}" placeholder="Tulis Nomor Whatsapp Utama (cth: 081957381623)" />
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>Facebook</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa-brands fa-facebook"></i></span>
                                <input type="text" class="form-control form-control-sm" name="facebook" value="{{ isset($socmed['facebook']) ? $socmed['facebook'] : null }}" placeholder="Tulis username akun facebook" />
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>Instagram</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa-brands fa-instagram"></i></span>
                                <input type="text" class="form-control form-control-sm" name="instagram" value="{{ isset($socmed['instagram']) ? $socmed['instagram'] : null }}" placeholder="Tulis username akun instagram" />
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>Twitter</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa-brands fa-twitter"></i></span>
                                <input type="text" class="form-control form-control-sm" name="twitter" value="{{ isset($socmed['twitter']) ? $socmed['twitter'] : null }}" placeholder="Tulis username akun twitter" />
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>TikTok</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa-brands fa-tiktok"></i></span>
                                <input type="text" class="form-control form-control-sm" name="tiktok" value="{{ isset($socmed['tiktok']) ? $socmed['tiktok'] : null }}" placeholder="Tulis username akun tiktok" />
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label>Youtube</label>
                            <div class="input-group">
                                <span class="input-group-text"><i class="fa-brands fa-youtube"></i></span>
                                <input type="text" class="form-control form-control-sm" name="youtube" value="{{ isset($socmed['youtube']) ? $socmed['youtube'] : null }}" placeholder="Tulis username akun youtube" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Socmed Modal --}}

    {{-- Payment Method Modal --}}
    <div class="modal fade" id="paymentMethodModal" tabindex="-1" aria-labelledby="paymentMethodModalLabel" aria-hidden="true">
        <form action="admin/settings/update-payment-method" autocomplete="off" method="POST" id="ajax-payment-method">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="paymentMethodModalLabel">Pengaturan Metode Pembayaran</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-dark table-hover">
                            <thead>
                                <tr>
                                    <th><i class="fa-solid fa-arrow-down-1-9"></i></th>
                                    <th>Nama</th>
                                    <th>Value</th>
                                    <th class="text-center">Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($payment_method->isNotEmpty())
                                    @foreach ($payment_method as $i => $val)
                                        @php
                                            $i++;
                                        @endphp
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $val['description'] . ' - ' . strtoupper($val['name']) }}</td>
                                            <td>{{ $val['value'] }}</td>
                                            <td class="text-center"><a href="" class="delete-payment-method" data-pk="{{ $val['id'] }}" data-url="admin/settings/delete-payment-method"><i class="fa-regular fa-trash-can"></i></a></td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="text-center">Data Kosong</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <div class="form-group mb-4">
                            <label>Pilih Metode Pembayaran</label>
                            <select class="form-select form-control form-control-sm" id="selectPaymentMethod" name="name" style="width: 100%" required>
                                <option value=""></option>
                                <option value="cod" data-val="Bayar Ditempat">Bayar Ditempat / COD</option>
                                <option value="lainnya">Lainnya</option>
                                <optgroup label="Transfer Bank">
                                    <option value="anz" data-val="Transfer Bank">ANZ</option>
                                    <option value="bca" data-val="Transfer Bank">BCA</option>
                                    <option value="bjb" data-val="Transfer Bank">BJB</option>
                                    <option value="bni" data-val="Transfer Bank">BNI</option>
                                    <option value="bri" data-val="Transfer Bank">BRI</option>
                                    <option value="btn" data-val="Transfer Bank">BTN</option>
                                    <option value="btpn" data-val="Transfer Bank">BTPN</option>
                                    <option value="bukopin" data-val="Transfer Bank">BUKOPIN</option>
                                    <option value="cimb" data-val="Transfer Bank">CIMB</option>
                                    <option value="citi" data-val="Transfer Bank">CITI</option>
                                    <option value="danamon" data-val="Transfer Bank">DANAMON</option>
                                    <option value="dbs" data-val="Transfer Bank">DBS</option>
                                    <option value="hsbc" data-val="Transfer Bank">HSBC</option>
                                    <option value="mandiri" data-val="Transfer Bank">MANDIRI</option>
                                    <option value="maybank" data-val="Transfer Bank">MAYBANK</option>
                                    <option value="mega" data-val="Transfer Bank">MEGA</option>
                                    <option value="nisp" data-val="Transfer Bank">NISP</option>
                                    <option value="panin" data-val="Transfer Bank">PANIN</option>
                                    <option value="permata" data-val="Transfer Bank">PERMATA</option>
                                    <option value="standardchartered" data-val="Transfer Bank">STANDARD CHARTERED</option>
                                    <option value="uob" data-val="Transfer Bank">UOB</option>
                                </optgroup>
                                <optgroup label="E-Wallet">
                                    <option value="dana" data-val="E-Wallet">DANA</option>
                                    <option value="doku" data-val="E-Wallet">DOKU</option>
                                    <option value="flip" data-val="E-Wallet">FLIP</option>
                                    <option value="gopay" data-val="E-Wallet">GOPAY</option>
                                    <option value="linkaja" data-val="E-Wallet">LINKAJA</option>
                                    <option value="ovo" data-val="E-Wallet">OVO</option>
                                    <option value="shopeepay" data-val="E-Wallet">SHOPEEPAY</option>
                                </optgroup>
                                <optgroup label="INSTALLMENT">
                                    <option value="akulaku" data-val="Installment">AKULAKU</option>
                                    <option value="americanexpress" data-val="Installment">AMERICAN EXPRESS</option>
                                    <option value="homecredit" data-val="Installment">HOME CREDIT</option>
                                    <option value="jcb" data-val="Installment">JCB</option>
                                    <option value="kredivo" data-val="Installment">KREDIVO</option>
                                    <option value="master" data-val="Installment">MASTER CARD</option>
                                    <option value="visa" data-val="Installment">VISA</option>
                                </optgroup>
                            </select>
                            <input type="hidden" name="description" id="descriptionPaymentMethod">
                        </div>
                        <div class="form-group mb-4 d-none" id="paymentLainnya">
                            <label>Nama Metode Pembayaran</label>
                            <input type="text" class="form-control form-control-sm" name="lainnya" placeholder="Masukkan nama metode pembayaran lainnya">
                        </div>
                        <div class="form-group mb-4">
                            <label>Value Metode Pembayaran</label>
                            <input type="text" class="form-control form-control-sm" id="valuePaymentMethod" name="value" placeholder="Masukkan Isi dari metode pembayaran">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-outline-primary">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{-- End Payment Method Modal --}}
@endsection

@section('importfootAppend')
    <link rel="stylesheet" href="css/arsdash/leaflet.css">
    <link rel="stylesheet" href="css/arsdash/esri-leaflet-geocoder.css">
    <script src="js/arsdash/leaflet.js"></script>
    <script src="js/arsdash/esri-leaflet.js"></script>
    <script src="js/arsdash/esri-leaflet-geocoder.js"></script>
    <script src="js/arsdash/bootstrap-editable.js"></script>
    <script src="js/arsdash/select2.min.js"></script>
    <script>
        $(document).ready(function() {

            $.fn.editable.defaults.mode = 'inline';
            $.fn.editable.defaults.inputclass = 'form-control form-control-sm';

            $(".editable").editable({
                ajaxOptions: {
                    type: "POST",
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                },
                success: function(response) {
                    if (response.status == "success") {
                        toastr.success(response.message, 'Success !', {
                            positionClass: "toast-top-center",
                            closeButton: true,
                            progressBar: true,
                            timeOut: 1000
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    } else {
                        toastr.error(response.message, 'Failed !', {
                            closeButton: true,
                            positionClass: "toast-top-center"
                        });
                    }
                }
            });

            $(".form-image").change(function() {
                var thumb = $(this).parent('.form-group').find('img');
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        thumb.attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });

            $("#ajax-logo").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });

            $("#ajax-hero").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });

            $("#ajax-time").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });

            $('.delete-time').click(function(e) {
                e.preventDefault();
                $thisParent = $(this).parents('tr');
                $.ajax({
                    beforeSend: function() {
                        $(this).addClass("disabled").html(
                            "<span aria-hidden='true' class='spinner-border spinner-border-sm' role='status'></span> Loading ..."
                        ).prop("disabled", "disabled");
                    },
                    type: "POST",
                    url: $(this).data('url'),
                    data: {
                        id: $(this).data('pk')
                    },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if (response.status === "success") {
                            toastr.success(response.message, 'Success !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                            setTimeout(function() {
                                $thisParent.remove();
                            }, 1500);
                        } else {
                            toastr.error((response.message ? response.message :
                                "Please complete your form"), 'Failed !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                        }
                    },
                    error: function(response) {
                        toastr.error(response.responseJSON.message, 'Failed !', {
                            closeButton: true,
                            progressBar: true,
                            timeOut: 1500
                        });
                    }
                });
            });

            $("#ajax-pinpoint").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });

            $("#ajax-socmed").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });

            function formatIcon(state) {
                if (!state.id) {
                    return state.text;
                }
                var baseUrl = "images/payment";
                var $state = $(
                    '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" style="width:40px;" /> ' + state.text + '</span>'
                );
                return $state;
            };

            $('#selectPaymentMethod').select2({
                placeholder: 'Please Select One',
                width: 'resolve',
                dropdownParent: $("#paymentMethodModal"),
                templateResult: formatIcon
            }).change(function(e) {
                $value = $(this).val();
                if ($value == 'lainnya') {
                    $('#descriptionPaymentMethod').val('lainnya');
                    $('#paymentLainnya').removeClass('d-none');
                    $('#valuePaymentMethod').prop('readonly', false);
                    $('#valuePaymentMethod').val('');
                } else if ($value == 'cod') {
                    $('#descriptionPaymentMethod').val($(this).find('option:selected').data('val'));
                    $('#paymentLainnya').addClass('d-none');
                    $('#valuePaymentMethod').val('Bayar Ditempat');
                    $('#valuePaymentMethod').prop('readonly', true);
                } else {
                    $('#paymentLainnya').addClass('d-none');
                    $('#descriptionPaymentMethod').val($(this).find('option:selected').data('val'));
                    $('#valuePaymentMethod').prop('readonly', false);
                    $('#valuePaymentMethod').val('');
                }
            });

            $("#ajax-payment-method").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });

            $('.delete-payment-method').click(function(e) {
                e.preventDefault();
                $thisParent = $(this).parents('tr');
                $.ajax({
                    beforeSend: function() {
                        $(this).addClass("disabled").html(
                            "<span aria-hidden='true' class='spinner-border spinner-border-sm' role='status'></span> Loading ..."
                        ).prop("disabled", "disabled");
                    },
                    type: "POST",
                    url: $(this).data('url'),
                    data: {
                        id: $(this).data('pk')
                    },
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        if (response.status === "success") {
                            toastr.success(response.message, 'Success !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                            setTimeout(function() {
                                $thisParent.remove();
                            }, 1500);
                        } else {
                            toastr.error((response.message ? response.message :
                                "Please complete your form"), 'Failed !', {
                                closeButton: true,
                                progressBar: true,
                                timeOut: 1500
                            });
                        }
                    },
                    error: function(response) {
                        toastr.error(response.responseJSON.message, 'Failed !', {
                            closeButton: true,
                            progressBar: true,
                            timeOut: 1500
                        });
                    }
                });
            });

            if ($('#pilihanWaktu:checked').val() != '1') {
                $('input[name="pilihan_tipe_waktu"]').prop("disabled", true);
            } else {
                $('input[name="pilihan_tipe_waktu"]').prop("disabled", false);
            }

            $('#pilihanWaktu').change(function(e) {
                var pos = $('#pilihanWaktu:checked').val();
                if (pos != '1') {
                    $('input[name="pilihan_tipe_waktu"]').prop("disabled", true);
                } else {
                    $('input[name="pilihan_tipe_waktu"]').prop("disabled", false);
                }
            });

        });

        const pinPointModalEl = document.getElementById('pinPointModal')
        pinPointModalEl.addEventListener('show.bs.modal', event => {

            var latLngInput = $("#latlng");
            var latLngInputVal = latLngInput.val();

            $('#mapid').html('<div id="showMap" style="height: 400px; width:100%; margin-bottom: 10px;"></div>');

            setTimeout(function() {
                var redIcon = new L.Icon({
                    iconUrl: 'css/arsdash/images/marker-icon-2x-red.png',
                    shadowUrl: 'css/arsdash/images/marker-shadow.png',
                    iconSize: [35, 51],
                    iconAnchor: [22, 51],
                    popupAnchor: [2, -44],
                    shadowSize: [51, 51]
                });

                if (latLngInputVal === '') {
                    var mymap = L.map('showMap').setView([-5.4286665, 105.188336], 12);
                    var marker = L.marker([-5.4286665, 105.188336], {
                        draggable: true,
                        icon: redIcon
                    });
                } else {
                    var mymap = L.map('showMap').setView([latLngInputVal.split(",")[0], latLngInputVal.split(",")[1]], 12);
                    var marker = L.marker([latLngInputVal.split(",")[0], latLngInputVal.split(",")[1]], {
                        draggable: true,
                        icon: redIcon
                    });
                }

                L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                    maxZoom: 20,
                    subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
                }).addTo(mymap);

                marker.on('dragend', function(e) {
                    latLngInput.val(e.target._latlng.lat.toFixed(6) + "," + e.target._latlng.lng.toFixed(6));
                    var lat = e.target._latlng.lat.toFixed(6);
                    var lng = e.target._latlng.lng.toFixed(6);
                    var newLatLng = new L.LatLng(lat, lng);
                    marker.setLatLng(newLatLng);
                    $('.checkMapVal > .empty').addClass('d-none');
                    $('.checkMapVal > .filled').removeClass('d-none');
                    $('.checkMapVal > .filled').attr('href', 'https://maps.google.com/?q=' + lat + ',' + lng);
                });

                function changeCoord() {
                    try {
                        var e = latLngInput.val();
                        var lat = e.split(",")[0];
                        var lng = e.split(",")[1];
                        var newLatLng = new L.LatLng(lat, lng);
                        marker.setLatLng(newLatLng);
                        mymap.setView(newLatLng);
                        $('.checkMapVal > .empty').addClass('d-none');
                        $('.checkMapVal > .filled').removeClass('d-none');
                        $('.checkMapVal > .filled').attr('href', 'https://maps.google.com/?q=' + lat + ',' + lng);
                    } catch (e) {
                        console.log("error");
                    }
                }

                var searchControl = L.esri.Geocoding.geosearch().addTo(mymap);
                var results = L.layerGroup().addTo(mymap);

                $('.geocoder-control-input').click(function() {
                    mymap.setZoom('11');
                });

                searchControl.on("results", function(data) {
                    $("#latlng").val(data.latlng.lat.toFixed(6) + ',' + data.latlng.lng.toFixed(6));
                    changeCoord();
                });

                marker.addTo(mymap);

                $('.leaflet-control-attribution.leaflet-control').html('&copy; <a href="http://www.arsieaziz.github.io/">arsieaziz</a>');

                function showLocation(position) {
                    var latitude = position.coords.latitude.toFixed(6);
                    var longitude = position.coords.longitude.toFixed(6);
                    $('#latlng').val(latitude + ',' + longitude);
                    changeCoord();
                }

                function errorHandler(err) {
                    if (err.code == 1) {
                        toastr.error("Akses Lokasi / GPS di Block!", 'Failed !', {
                            closeButton: true,
                            progressBar: true,
                            timeOut: 1500
                        });
                    } else if (err.code == 2) {
                        toastr.error("Position is unavailable!", 'Failed !', {
                            closeButton: true,
                            progressBar: true,
                            timeOut: 1500
                        });
                    }
                }

                function getLocation() {
                    if (navigator.geolocation) {
                        var options = {
                            timeout: 60000
                        };
                        navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);
                    } else {
                        alert("Sorry, browser does not support geolocation!");
                    }
                }
                $('#getLocateNow').click(function() {
                    getLocation();
                });
            }, 500);
        });
    </script>
@endsection
