@extends('frontend.layouts.auth')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
@endsection

@section('content')
    <section class="auth">
        <div class="container">
            <div class="head">
                <a class="logo">
                    @if (isset($logo['image']['value']) && !empty($logo['image']['value']))
                        <img src="{{ 'storage/images/original/' . $logo['image']['value'] }}" alt="">
                    @endif
                    @if (!empty($logo['text_first']['value']) || !empty($logo['text_second']['value']))
                        <div class="text">
                            @if (!empty($logo['text_first']['value']))
                                <h1 style="color: {{ $logo['text_first']['description'] }};">{{ $logo['text_first']['value'] }}</h1>
                            @endif
                            @if (!empty($logo['text_second']['value']))
                                <h1 style="color: {{ $logo['text_second']['description'] }};">{{ $logo['text_second']['value'] }}</h1>
                            @endif
                        </div>
                    @endif
                </a>
            </div>
            <div class="body">
                <div class="card">
                    <a href="/" class="back"><i class="fas fa-arrow-left-long"></i> Beranda</a>
                    <h1>Masuk <span class="d-block">Ke akun anda</span></h1>
                    <form action="login" autocomplete="off" method="POST" id="ajax">
                        @csrf
                        <div class="mb-3">
                            <label for="nohpInput" class="form-label">Nomor Handphone / Whatsapp</label>
                            <input type="text" name="number_phone" pattern="^(0)8[1-9][0-9]{6,9}$" class="form-control form-control-sm" id="nohpInput" placeholder="Isi Nomor Anda (cth: 081957381623)">
                        </div>
                        <div class="mb-3">
                            <label for="passwordInput" class="form-label">Password</label>
                            <div class="input-group input-group-sm">
                                <input type="password" name="password" class="form-control form-control-sm" id="passwordInput" placeholder="Isi Password Anda">
                                <span class="input-group-text p-0"><button class="btn btn-sm btn-show-password" type="button"><i class="far fa-eye"></i></button></span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <a href="" class="forgot-password" target="_blank">Lupa Kata Sandi?</a>
                            <button type="submit" class="btn btn-login"><i class="fas fa-sign-in-alt"></i> Masuk</button>
                        </div>
                    </form>
                    <p class="question">Belum memiliki akun? <a href="registrasi">Daftar Disini</a></p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/jquery.validate.min.js"></script>
    <script src="js/arsdash/toastr.min.js"></script>
    <script>
        $('section.auth').append('<div class="wave"></div><div class="wave"></div><div class="wave"></div>');
        $(document).ready(function() {
            $("#ajax").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (response.status == 'success') {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.error(response.message, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
