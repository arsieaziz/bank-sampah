@extends('frontend.layouts.auth')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/flatpickr.min.css">
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
@endsection

@section('content')
    <section class="auth">
        <div class="container">
            <div class="head">
                <a class="logo">
                    @if (isset($logo['image']['value']) && !empty($logo['image']['value']))
                        <img src="{{ 'storage/images/original/' . $logo['image']['value'] }}" alt="">
                    @endif
                    @if (!empty($logo['text_first']['value']) || !empty($logo['text_second']['value']))
                        <div class="text">
                            @if (!empty($logo['text_first']['value']))
                                <h1 style="color: {{ $logo['text_first']['description'] }};">{{ $logo['text_first']['value'] }}</h1>
                            @endif
                            @if (!empty($logo['text_second']['value']))
                                <h1 style="color: {{ $logo['text_second']['description'] }};">{{ $logo['text_second']['value'] }}</h1>
                            @endif
                        </div>
                    @endif
                </a>
            </div>
            <div class="body">
                <div class="card registration">
                    <a href="/" class="back"><i class="fas fa-arrow-left-long"></i> Beranda</a>
                    <h1>Profile <span class="d-block">{{ Auth::guard('member')->user()->number_phone }}</span></h1>
                    <div class="row">
                        <div class="col-md-6">
                            <form action="profil" autocomplete="off" method="POST" id="ajaxProfile">
                                @csrf
                                @method('PUT')
                                <div class="card registration mb-4 mb-md-0 p-3">
                                    <div class="mb-3">
                                        <label for="namaInput" class="form-label">Nama Lengkap</label>
                                        <input type="text" name="fullname" class="form-control form-control-sm" id="namaInput" placeholder="Isi Nomor Anda (cth: Joko Santoso)" value="{{ Auth::guard('member')->user()->fullname }}">
                                    </div>
                                    <div class="mb-3">
                                        <label for="tlInput" class="form-label">Tanggal Lahir</label>
                                        <div class="input-group input-group-sm selectTlDate">
                                            <input type="text" name="date_birth" class="form-control form-control-sm" id="ttlInput" placeholder="Pilih Tanggal Lahir" data-input value="{{ Auth::guard('member')->user()->date_birth }}<">
                                            <span class="input-group-text" title="toggle" data-toggle><i class="fa fa-calendar-alt"></i></span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-login"><i class="fas fa-pen-to-square"></i> Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form action="change-password" autocomplete="off" method="POST" id="ajaxPassword">
                                @csrf
                                @method('PUT')
                                <div class="card registration mb-0 p-3">
                                    <div class="mb-3">
                                        <label for="oldInput" class="form-label">Password Lama</label>
                                        <div class="input-group input-group-sm">
                                            <input type="password" name="old_password" class="form-control form-control-sm" id="oldInput" placeholder="Isi Password Lama Anda">
                                            <span class="input-group-text p-0"><button class="btn btn-sm btn-show-password" type="button"><i class="far fa-eye"></i></button></span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="passwordInput" class="form-label">Password</label>
                                        <div class="input-group input-group-sm">
                                            <input type="password" name="password" class="form-control form-control-sm" id="passwordInput" placeholder="Isi Password Anda">
                                            <span class="input-group-text p-0"><button class="btn btn-sm btn-show-password" type="button"><i class="far fa-eye"></i></button></span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-update"><i class="fas fa-pen-to-square"></i> Ubah Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/jquery.validate.min.js"></script>
    <script src="js/arsdash/toastr.min.js"></script>
    <script src="js/arsdash/flatpickr.min.js"></script>
    <script src="js/arsdash/flatpickr-id.js"></script>
    <script>
        $('section.auth').append('<div class="wave"></div><div class="wave"></div><div class="wave"></div>');
        $(".selectTlDate").flatpickr({
            maxDate: "today",
            enableTime: false,
            dateFormat: "Y-m-d",
            wrap: true,
            locale: "id",
            disableMobile: true
        });
        $(document).ready(function() {
            $("#ajaxProfile").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    if (response.redirect == "" || response
                                        .redirect == "reload") {
                                        location.reload();
                                    } else {
                                        location.href = response.redirect;
                                    }
                                }, 1500);
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    if (i != 'active') {
                                        toastr.error(i + ': ' + field, 'Failed !', {
                                            closeButton: true,
                                            progressBar: true,
                                            timeOut: 1500
                                        });
                                    }
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });
            $("#ajaxPassword").validate({
                highlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                        'd-block');
                },
                unhighlight: function(element) {
                    $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                        'd-none');
                },
                errorElement: 'small',
                errorClass: 'text-danger',
                errorPlacement: function(error, element) {
                    error.appendTo(element.closest('.form-group'));
                },
                submitHandler: function(form, eve) {
                    eve.preventDefault();
                    var myform = $(form);
                    var btnSubmit = myform.find("[type='submit']");
                    var btnSubmitHtml = btnSubmit.html();
                    var url = myform.attr("action");
                    var method = myform.attr("method");
                    var data = new FormData(form);
                    $.ajax({
                        beforeSend: function() {
                            btnSubmit.addClass("disabled").html(
                                "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                            );
                        },
                        cache: false,
                        processData: false,
                        contentType: false,
                        type: method,
                        url: url,
                        data: data,
                        dataType: 'JSON',
                        success: function(response) {
                            if (!response.error) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                if (response.status == 'error') {
                                    toastr.error(response.message, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                } else {
                                    toastr.success(response.message, 'Success !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1500
                                    });
                                    setTimeout(function() {
                                        if (response.redirect == "" || response
                                            .redirect == "reload") {
                                            location.reload();
                                        } else {
                                            location.href = response.redirect;
                                        }
                                    }, 1500);
                                }
                            } else {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    if (i != 'active') {
                                        toastr.error(i + ': ' + field, 'Failed !', {
                                            closeButton: true,
                                            progressBar: true,
                                            timeOut: 1500
                                        });
                                    }
                                });
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Failed !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
