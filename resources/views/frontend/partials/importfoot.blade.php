<script src="js/frontend/vendor.min.js"></script>
<script src="js/frontend/main.min.js"></script>
<script>
    $(window).on("load", (function(e) {
        setTimeout((function() {
            $(".preloader-wrapper").fadeOut(1000);
        }), 1000)
    }))
</script>
@yield('importfootAppend')
