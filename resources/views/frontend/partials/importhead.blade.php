<link rel="stylesheet" href="css/frontend/vendor.min.css">
<link rel="stylesheet" href="css/frontend/style1.min.css">
<style>
    .preloader-wrapper {
        width: 100%;
        height: 100vh;
        position: fixed;
        z-index: 99999;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        background: rgba(255, 255, 255, 1)
    }

    .preloader-wrapper .preloader img {
        height: 100px;
    }

    #whatsappShortcut {
        position: fixed;
        z-index: 1399;
        bottom: 70px;
        width: 40px;
        height: 40px;
        right: 20px;
        background: linear-gradient(to bottom, rgba(37, 211, 101, 1) 0%, rgba(18, 140, 126, 1) 100%);
        border: none;
        border-radius: 5px;
        color: #fff !important;
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-transition: .5s;
        transition: .5s;
    }

    #whatsappShortcut.active {
        bottom: 120px;
    }
</style>
@yield('importheadAppend')
