    <meta property="og:type" content="Website" />
    <meta property="og:title" content="{{ $settings['web_title'] }}">
    <meta property="og:description" content="{{ $settings['web_description'] }}">
    <meta property="og:image" itemprop="image" content="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <meta property="og:image:secure_url" itemprop="image" content="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <meta property="og:url" content="{{ url('') }}">
    <meta property="og:site_name" content="{{ $settings['web_title'] }}">

    <meta name="twitter:title" content="{{ $settings['web_title'] }}">
    <meta name="twitter:description" content="{{ $settings['web_description'] }}">
    <meta name="twitter:image" content="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <meta name="twitter:image:alt" content="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">

    <link rel="icon" type="image/x-icon" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <meta name="msapplication-TileColor" content="#fff">
    <meta name="msapplication-TileImage" content="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}">
    <meta name="theme-color" content="#fff">
    @yield('metadataAppend')
