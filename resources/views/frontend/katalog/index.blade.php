@extends('frontend.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/select2.min.css">
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
    <style>
        .active>.page-link,
        .page-link.active {
            background-color: #21a1c2;
            border-color: #21a1c2;
        }
    </style>
@endsection

@section('content')
    <section class="page-header" style="background-image: url({{ 'storage/images/thumbnail/' . $hero['image'] }});">
        <div class="container">
            <div class="text">
                <h1>Katalog Sampah</h1>
                <p><a href="/"><i class="fa-solid fa-arrow-left-long"></i> Kembali</a></p>
            </div>
        </div>
    </section>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section class="catalogue">
        <div class="container">
            <div class="info">
                <p class="m-0">Menampilkan <span class="fw-bold">{{ $data->count() }}</span> dari total <span class="fw-bold">{{ $data->total() }}</span> produk</p>
                <div class="sort">
                    <label class="me-1">Urutkan :</label>
                    <select class="form-control form-control-sm" id="sortSelect">
                        <option value="terbaru" {{ request()->orderBy == 'created_at' && request()->sortedBy == 'desc' ? 'selected' : null }}>Terbaru</option>
                        <option value="terlama" {{ request()->orderBy == 'created_at' && request()->sortedBy == 'asc' ? 'selected' : null }}>Terlama</option>
                        <option value="termurah" {{ request()->orderBy == 'price' && request()->sortedBy == 'asc' ? 'selected' : null }}>Harga Termurah</option>
                        <option value="termahal" {{ request()->orderBy == 'price' && request()->sortedBy == 'desc' ? 'selected' : null }}>Harga Termahal</option>
                    </select>
                </div>
            </div>
            <ul class="list-product row">
                @if (!empty($data) && $data->count())
                    @foreach ($data as $key => $value)
                        <li class="col-6 col-md-4 col-xl-3 mb-4">
                            <a href="katalog/{{ $value->slug }}" class="card">
                                <img src="storage/images/thumbnail/{{ $value->main_image }}" alt="">
                                <div class="text">
                                    <h1 class="productTitle">{{ $value->name }}</h1>
                                    <h2 class="productPrice">Rp{{ number_format($value->price, 0, '', '.') }}/Kg</h2>
                                </div>
                            </a>
                        </li>
                    @endforeach
                @else
                    <li class="col-12 mb-0">Tidak ada data yang sesuai dengan pencarian anda.</li>
                @endif
            </ul>
            {!! $data->appends(Request::all())->links() !!}
        </div>
    </section>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/select2.min.js"></script>
    <script src="js/arsdash/toastr.min.js"></script>
    <script>
        function insertParam(key, value) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);
            var kvp = document.location.search.substr(1).split('&');
            let i = 0;
            for (; i < kvp.length; i++) {
                if (kvp[i].startsWith(key + '=')) {
                    let pair = kvp[i].split('=');
                    pair[1] = value;
                    kvp[i] = pair.join('=');
                    break;
                }
            }
            if (i >= kvp.length) {
                kvp[kvp.length] = [key, value].join('=');
            }
            let params = kvp.join('&');
            document.location.search = params;
        }

        function insertMultiParam(key, value, key2, value2) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);
            key2 = encodeURIComponent(key2);
            value2 = encodeURIComponent(value2);
            const urlParams = new URLSearchParams(window.location.search);
            urlParams.set(key, value);
            urlParams.set(key2, value2);
            document.location.search = urlParams;
        }
        $(document).ready(function() {
            $("#sortSelect").select2({
                allowClear: false
            }).change(function() {
                var val = $(this).val();
                if (val == 'terbaru') {
                    insertMultiParam('orderBy', 'created_at', 'sortedBy', 'desc');
                } else if (val == 'terlama') {
                    insertMultiParam('orderBy', 'created_at', 'sortedBy', 'asc');
                } else if (val == 'termurah') {
                    insertMultiParam('orderBy', 'price', 'sortedBy', 'asc');
                } else if (val == 'termahal') {
                    insertMultiParam('orderBy', 'price', 'sortedBy', 'desc');
                }
            });
        });
    </script>
@endsection
