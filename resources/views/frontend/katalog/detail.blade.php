@extends('frontend.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/swiper-bundle.min.css">
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
@endsection

@section('content')
    <section class="page-header" style="background-image: url({{ 'storage/images/thumbnail/' . $hero['image'] }});">
        <div class="container">
            <div class="text">
                <h1>Katalog Sampah</h1>
                <p><a href="katalog"><i class="fa-solid fa-arrow-left-long"></i> Kembali</a></p>
            </div>
        </div>
    </section>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section class="catalogue-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="productImage">
                        <div class="active">
                            <img src="{{ 'storage/images/thumbnail/' . $data->main_image }}" alt="">
                        </div>
                        <div class="listImage swiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide active"><img src="{{ 'storage/images/thumbnail/' . $data->main_image }}" class="img-fluid" alt=""></div>
                                @if (isset($photo) && !empty($photo))
                                    @foreach ($photo as $item)
                                        <div class="swiper-slide"><img src="{{ 'storage/images/thumbnail/' . $item->value }}" class="img-fluid" alt=""></div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="listImage-next"><i class="fa-solid fa-angle-right"></i></div>
                            <div class="listImage-prev"><i class="fa-solid fa-angle-left"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="productInfo">
                        <h1 class="title">{{ $data->name }}</h1>
                        <h1 class="price">Rp{{ number_format($data->price, 0, '', '.') }}/Kg</h1>
                        <ul class="product-share">
                            <li><span>Bagikan :</span></li>
                            <li><a href="" class="facebook" target="_blank"><i class="fa-brands fa-facebook"></i></a></li>
                            <li><a href="" class="twitter" target="_blank"><i class="fa-brands fa-twitter"></i></a></li>
                            <li><a href="" class="whatsapp" target="_blank"><i class="fa-brands fa-whatsapp"></i></a></li>
                            <li><a href="" class="copyClipboard"><i class="fa-solid fa-copy"></i></a></li>
                        </ul>
                        <div class="content">
                            {!! $data->description !!}
                        </div>
                        <hr>
                        <div class="d-block">
                            <label class="fw-bold">Masukkan berat sampah :</label>
                            <div class="input-group mb-3" style="width: 30%;">
                                <input type="number" class="form-control form-control-sm" min="0.01" step="0.01" placeholder="0" id="qtySampah">
                                <span class="input-group-text">/Kg</span>
                            </div>
                        </div>
                        <div class="d-none" id="konversi">
                            <label class="fw-bold">Hasil Konversi :</label>
                            <p id="konversiPrice" style="font-size: 24px; font-weight: bold; color: #4fb2af;">-</p>
                        </div>
                        <div class="btn-buy d-none">
                            <a href="keranjang" class="btn btn-buy-direct" data-img="{{ 'storage/images/thumbnail/' . $data->main_image }}" data-title="{{ $data->name }}" data-id="{{ $data->id }}" data-url="{{ 'katalog/' . $data->slug }}" data-price="{{ $data->price }}" data-qty="" data-pay="">Jual Langsung</a>
                            <button class="btn btn-buy-cart addToCart" data-img="{{ 'storage/images/thumbnail/' . $data->main_image }}" data-title="{{ $data->name }}" data-id="{{ $data->id }}" data-url="{{ 'katalog/' . $data->slug }}" data-price="{{ $data->price }}" data-qty="" data-pay=""><i class="fa-solid fa-plus"></i> Keranjang</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('importfootAppend')
    <script src="https://cdn.rawgit.com/igorescobar/jQuery-Mask-Plugin/1ef022ab/dist/jquery.mask.min.js"></script>
    <script src="js/arsdash/swiper-bundle.min.js"></script>
    <script src="js/arsdash/jquery.zoom.js"></script>
    <script src="js/arsdash/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            var formatRp = function(num) {
                var str = num.toString().replace("", ""),
                    parts = false,
                    output = [],
                    i = 1,
                    formatted = null;
                if (str.indexOf(".") > 0) {
                    parts = str.split(".");
                    str = parts[0];
                }
                str = str.split("").reverse();
                for (var j = 0, len = str.length; j < len; j++) {
                    if (str[j] != ".") {
                        output.push(str[j]);
                        if (i % 3 == 0 && j < (len - 1)) {
                            output.push(".");
                        }
                        i++;
                    }
                }
                formatted = output.reverse().join("");
                return ("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
            };

            $('#qtySampah').keyup(function() {
                var val = $(this).val();
                $('.btn-buy-direct').data('qty', 0);
                $('.btn-buy-direct').data('pay', 0);
                $('.btn-buy-cart').data('qty', 0);
                $('.btn-buy-cart').data('pay', 0);
                $('.btn-buy').addClass('d-none');
                $('#konversiPrice').html('-');
                $('#konversi').addClass('d-none');
                if (val > 0.00) {
                    var price = $('.btn-buy-direct').data('price');
                    $('.btn-buy-direct').data('qty', val);
                    $('.btn-buy-direct').data('pay', (price * val));
                    $('.btn-buy-cart').data('qty', val);
                    $('.btn-buy-cart').data('pay', (price * val));
                    $('#konversiPrice').html('Rp' + formatRp(price * val) + ',-');
                    $('#konversi').removeClass('d-none');
                    $('.btn-buy').removeClass('d-none');
                }
            })

            var oldvalProductImage = $('.productImage > .active > img').attr('src');
            $('.productImage > .listImage > .swiper-wrapper > .swiper-slide > img').hover(function() {
                var valImage = $(this).attr('src');
                $('.productImage > .active > img').attr('src', valImage);
            }, function() {
                $('.productImage > .active > img').attr('src', oldvalProductImage);
                $('.productImage > .active').trigger('zoom.destroy');
                $('.productImage > .active').zoom();
            });

            $('.productImage > .listImage > .swiper-wrapper > .swiper-slide > img').click(function() {
                var valImage = $(this).attr('src');
                oldvalProductImage = valImage;
                $(this).parents('.swiper-wrapper').find('.active').removeClass('active');
                $(this).parent('.swiper-slide').addClass('active');
                $('.productImage > .active > img').attr('src', valImage);
            });

            $('.productImage > .active').zoom();

            $('.productImage > .active').hover(function() {
                $(this).zoom();
            });
            var swiper = new Swiper(".listImage", {
                slidesPerView: 4,
                spaceBetween: 10,
                navigation: {
                    nextEl: ".listImage-next",
                    prevEl: ".listImage-prev",
                },
            });
        });
    </script>
@endsection
