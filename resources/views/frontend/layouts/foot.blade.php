<footer>
    <div class="top">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <a class="logo">
                        @if (isset($logo['image']['value']) && !empty($logo['image']['value']))
                            <img src="{{ 'storage/images/original/' . $logo['image']['value'] }}" alt="">
                        @endif
                        @if (!empty($logo['text_first']['value']) || !empty($logo['text_second']['value']))
                            <div class="text">
                                @if (!empty($logo['text_first']['value']))
                                    <h1 style="color: {{ $logo['text_first']['description'] }};">{{ $logo['text_first']['value'] }}</h1>
                                @endif
                                @if (!empty($logo['text_second']['value']))
                                    <h1 style="color: {{ $logo['text_second']['description'] }};">{{ $logo['text_second']['value'] }}</h1>
                                @endif
                            </div>
                        @endif
                    </a>
                    <p>{{ $settings['web_description'] }}</p>
                    <div class="socmed">
                        @if (!empty($socmed['whatsapp']) && $socmed['whatsapp'] != '-')
                            <a href="{{ 'https://wa.me/62' . substr($socmed['whatsapp'], 1) }}" target="_blank"><i class="fa-brands fa-square-whatsapp"></i></a>
                        @endif
                        @if (!empty($socmed['facebook']) && $socmed['facebook'] != '-')
                            <a href="{{ 'https://facebook.com/' . $socmed['facebook'] }}" target="_blank"><i class="fa-brands fa-square-facebook"></i></a>
                        @endif
                        @if (!empty($socmed['instagram']) && $socmed['instagram'] != '-')
                            <a href="{{ 'https://instagram.com/' . $socmed['instagram'] }}" target="_blank"><i class="fa-brands fa-square-instagram"></i></a>
                        @endif
                        @if (!empty($socmed['twitter']) && $socmed['twitter'] != '-')
                            <a href="{{ 'https://twitter.com/' . $socmed['twitter'] }}" target="_blank"><i class="fa-brands fa-square-twitter"></i></a>
                        @endif
                        @if (!empty($socmed['youtube']) && $socmed['youtube'] != '-')
                            <a href="{{ 'https://youtube.com/' . $socmed['youtube'] }}" target="_blank"><i class="fa-brands fa-square-youtube"></i></a>
                        @endif
                        @if (!empty($socmed['tiktok']) && $socmed['tiktok'] != '-')
                            <a href="{{ 'https://tiktok.com/' . $socmed['tiktok'] }}" target="_blank"><i class="fa-brands fa-tiktok"></i></a>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <h1>Kontak Kami</h1>
                    <ul class="contact">
                        <li><a href="{{ 'tel:' . $settings['telp'] }}" target="_blank"><i class="fa-solid fa-phone"></i><span>{{ $settings['telp'] }}</span></a></li>
                        <li><a href="{{ 'mailto:' . $settings['email'] }}" target="_blank"><i class="fa-solid fa-envelope"></i><span>{{ $settings['email'] }}</span></a></li>
                        <li><a href="{{ 'https://maps.google.com/?q=' . $pinpoint }}" target="_blank"><i class="fa-solid fa-location-dot"></i><span>{{ $settings['address'] }}</span></a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h1>Jam Buka</h1>
                    <ul class="openHours">
                        @foreach ($open_time as $item)
                            <li><span>{{ $item['name'] }} :</span> {{ $item['description'] }}</li>
                        @endforeach
                    </ul>
                    <h1>Pembayaran</h1>
                    <div class="d-flex align-items-center">
                        @foreach ($payment as $item)
                            <img src="images/payment/{{ $item['name'] }}.png" alt="{{ $item['name'] }}" style="height: 30px; margin: 0 5px;">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom">
        <p class="text-center">&copy; {{ date('Y') . ' ' . $settings['web_title'] }} - Powered By <a href="https://arsieaziz.github.io">ArsieAziz</a></p>
    </div>
</footer>
<a href="https://api.whatsapp.com/send?phone=62{{ substr($socmed['whatsapp'], 1) }}&text=Halo,%20*{{ rawurlencode($settings['web_title']) }}*." target="_blank" id="whatsappShortcut"><i class="fa-brands fa-2x fa-whatsapp"></i></a>
<a href="keranjang" id="cartShortcut"><i class="fa-solid fa-cart-plus"></i><span class="cartCount bg-danger">0</span></a>
<button id="return-to-top"><i class="fas fa-chevron-up"></i></button>
@yield('footAppend')
