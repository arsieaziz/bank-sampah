<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>{{ $settings['web_title'] }}</title>
    <base href="{{ url('') }}" />

    <meta name="title" content="{{ $settings['web_title'] }}" />
    <meta name="description" content="{{ $settings['web_description'] }}" />
    <meta name="keyword" content="{{ $settings['web_keyword'] }}" />
    @include('frontend.partials.metadata')
    @include('frontend.partials.importhead')
    @laravelPWA
</head>

<body>
    <div class="preloader-wrapper">
        <div class="preloader">
            <img src="{{ url('storage/images/original') . '/' . $logo['image']['value'] }}" alt="Logo">
        </div>
    </div>
    <main>
        @yield('content')
    </main>
    @include('frontend.partials.importfoot')
</body>

</html>
