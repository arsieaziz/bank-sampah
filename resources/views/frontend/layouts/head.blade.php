<header id="overlay">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-9 col-lg-3">
                <a class="logo">
                    @if (isset($logo['image']['value']) && !empty($logo['image']['value']))
                        <img src="{{ 'storage/images/original/' . $logo['image']['value'] }}" alt="">
                    @endif
                    @if (!empty($logo['text_first']['value']) || !empty($logo['text_second']['value']))
                        <div class="text">
                            @if (!empty($logo['text_first']['value']))
                                <h1 style="color: {{ $logo['text_first']['description'] }};">{{ $logo['text_first']['value'] }}</h1>
                            @endif
                            @if (!empty($logo['text_second']['value']))
                                <h1 style="color: {{ $logo['text_second']['description'] }};">{{ $logo['text_second']['value'] }}</h1>
                            @endif
                        </div>
                    @endif
                </a>
            </div>
            <div class="col-3 col-lg-9 d-flex align-items-center justify-content-end">
                <div class="menu d-lg-flex align-items-center justify-content-end">
                    <div class="container">
                        <ul>
                            <li {{ request()->segment(1) == '' ? 'class=active' : null }}><a href="/">Beranda</a></li>
                            <li {{ request()->segment(1) == 'tentang' ? 'class=active' : null }}><a href="tentang">Tentang</a></li>
                            <li {{ request()->segment(1) == 'katalog' ? 'class=active' : null }}><a href="katalog">Katalog Sampah</a></li>
                            <li>
                                @if (!Auth::guard('member')->user())
                                    <div class="notLogin">
                                        <a href="login" class="first"><i class="fas fa-sign-in-alt"></i> Masuk</a><a href="registrasi" class="second"><i class="fas fa-pen-to-square"></i> Daftar</a>
                                    </div>
                                @else
                                    <div class="login">
                                        <a href="profil" class="first"><i class="fas fa-user-alt"></i> Profil</a><a href="javascript:void(0);" onclick="window.document.getElementById('formLogout').submit();" class="second" style="background: #dc3545 !important;border-color: #dc3545 !important;"><i class="fas fa-sign-out-alt"></i> Keluar</a>
                                    </div>
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="cage-nav d-flex d-lg-none">
                    <div class="navTrigger">
                        <i></i><i></i><i></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="formLogout" class="d-none" action="logout-member" method="POST">
        @csrf @method('POST')
    </form>
</header>
