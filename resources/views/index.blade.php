@extends('frontend.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
@endsection

@section('content')
    <section class="hero" style="background-image: url({{ 'storage/images/thumbnail/' . $hero['image'] }})">
        <div class="container">
            <div class="content" data-aos="fade-up" data-aos-duration="1500">
                <h1 class="title">{{ $hero['title'] }}</h1>
                <p>{{ $hero['description'] }}</p>
                <a href="katalog" class="btn btn-sm">Tukarkan Sampah Anda</a>
            </div>
        </div>
    </section>
    <section class="catalogue">
        <div class="container">
            <div class="content" data-aos="fade-up" data-aos-duration="1500">
                <h1 class="title">Jenis Sampah</h1>
                <ul class="list-product row">
                    @foreach ($produk_terbaru as $item => $val)
                        <li class="col-6 col-md-4 col-xl-3 mb-4">
                            <a href="{{ 'katalog/' . $val['slug'] }}" class="card">
                                <img src="{{ 'storage/images/thumbnail/' . $val['main_image'] }}" alt="">
                                <div class="text">
                                    <h1 class="productTitle">{{ $val['name'] }}</h1>
                                    <h2 class="productPrice">Rp{{ number_format($val->price, 0, '', '.') }}/Kg</h2>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
                <a href="katalog" class="btn btn-sm btn-load-more">Lihat Lainnya</a>
            </div>
        </div>
    </section>
@endsection

@section('importfootAppend')
    <script src="js/arsdash/toastr.min.js"></script>
@endsection
