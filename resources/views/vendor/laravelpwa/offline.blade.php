<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title>{{ $settings['web_title'] }}</title>
    <base href="{{ url('') }}" />

    <meta name="title" content="{{ $settings['web_title'] }}" />
    <meta name="description" content="{{ $settings['web_description'] }}" />
    <meta name="keyword" content="{{ $settings['web_keyword'] }}" />
    <link rel="stylesheet" href="css/frontend/vendor.min.css">
    <link rel="stylesheet" href="css/frontend/style1.min.css">
    <style>
        .preloader-wrapper {
            width: 100%;
            height: 100vh;
            position: fixed;
            z-index: 99999;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            background: rgba(255, 255, 255, 1)
        }

        .preloader-wrapper .preloader img {
            height: 100px;
        }

        #whatsappShortcut {
            position: fixed;
            z-index: 1399;
            bottom: 70px;
            width: 40px;
            height: 40px;
            right: 20px;
            background: linear-gradient(to bottom, rgba(37, 211, 101, 1) 0%, rgba(18, 140, 126, 1) 100%);
            border: none;
            border-radius: 5px;
            color: #fff !important;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-transition: .5s;
            transition: .5s;
        }

        #whatsappShortcut.active {
            bottom: 120px;
        }
    </style>
</head>

<body>
    <div class="preloader-wrapper">
        <div class="preloader">
            <img src="{{ asset('images/logo.png') }}" alt="Logo">
        </div>
    </div>
    <main>
        <section class="auth">
            <div class="container">
                <div class="head">
                    <a class="logo">
                        <img src="{{ asset('images/logo.png') }}" alt="">
                    </a>
                </div>
                <div class="body">
                    <div class="card">
                        <a href="/" class="back"><i class="fas fa-arrow-left-long"></i> Beranda</a>
                        <h1>Anda saat ini sedang offline</h1>
                    </div>
                </div>
            </div>
        </section>
        <script src="js/frontend/vendor.min.js"></script>
        <script src="js/frontend/main.min.js"></script>
        <script>
            $(window).on("load", (function(e) {
                setTimeout((function() {
                    $(".preloader-wrapper").fadeOut(1000);
                }), 1000)
            }))
        </script>
        <script>
            $('section.auth').append('<div class="wave"></div><div class="wave"></div><div class="wave"></div>');
        </script>
    </main>
</body>

</html>
