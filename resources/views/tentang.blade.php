@extends('frontend.layouts.main')

@section('importheadAppend')
@endsection

@section('content')
    <section class="page-header" style="background-image: url({{ 'storage/images/thumbnail/' . $hero['image'] }});">
        <div class="container">
            <div class="text">
                <h1>Tentang Kami</h1>
                <p><a href="/"><i class="fa-solid fa-arrow-left-long"></i> Kembali</a></p>
            </div>
        </div>
    </section>
    <section class="page">
        <div class="container">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="content">
                {!! $data->value !!}
            </div>
            <div class="share-view">
                <ul>
                    <li class="view"><a href=""><i class="fa-solid fa-eye"></i><span class="count">{{ $data->view + 1 }}</span></a></li>
                    <li class="facebook"><a href="" target="_blank"><i class="fa-brands fa-square-facebook"></i><span class="count">{{ $data->facebook }}</span></a></li>
                    <li class="twitter"><a href="" target="_blank"><i class="fa-brands fa-square-twitter"></i><span class="count">{{ $data->twitter }}</span></a></li>
                    <li class="whatsapp"><a href="" target="_blank"><i class="fa-brands fa-square-whatsapp"></i><span class="count">{{ $data->whatsapp }}</span></a></li>
                </ul>
            </div>
        </div>
    </section>
@endsection

@section('importfootAppend')
    <script>
        $(document).ready(function() {
            $('.share-view').find('.facebook > a').attr('href', 'http://facebook.com/sharer.php?u=' + window.location.href);
            $('.share-view').find('.twitter > a').attr('href', 'http://twitter.com/share?url=' + window.location.href);
            $('.share-view').find('.whatsapp > a').attr('href', 'https://api.whatsapp.com/send?text=%20Baca%20Ini%20:%20' + window.location.href);
            setTimeout(function() {
                $.ajax({
                    cache: false,
                    processData: false,
                    type: "PUT",
                    url: 'tentang/view/1',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }, 1000);
            $('.facebook').click(function() {
                $.ajax({
                    cache: false,
                    processData: false,
                    type: "PUT",
                    url: 'tentang/facebook/1',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
            $('.twitter').click(function() {
                $.ajax({
                    cache: false,
                    processData: false,
                    type: "PUT",
                    url: 'tentang/twitter/1',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
            $('.whatsapp').click(function() {
                $.ajax({
                    cache: false,
                    processData: false,
                    type: "PUT",
                    url: 'tentang/whatsapp/1',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
        });
    </script>
@endsection
