@extends('frontend.layouts.main')

@section('importheadAppend')
    <link rel="stylesheet" href="css/arsdash/select2.min.css">
    <link rel="stylesheet" href="css/arsdash/flatpickr.min.css">
    <link rel="stylesheet" href="css/arsdash/leaflet.css">
    <link rel="stylesheet" href="css/arsdash/esri-leaflet-geocoder.css">
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
@endsection

@section('content')
    <section class="page-header" style="background-image: url({{ 'storage/images/thumbnail/' . $hero['image'] }});">
        <div class="container">
            <div class="text">
                <h1>Keranjang</h1>
                <p><a href="katalog"><i class="fa-solid fa-arrow-left-long"></i> Kembali</a></p>
            </div>
        </div>
    </section>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <section class="cart">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <ul class="cartList">
                    </ul>
                </div>
                <div class="col-lg-5">
                    <div class="cartChekout">
                        <h1 class="title">Data Pribadi</h1>
                        @if (!Auth::guard('member')->user())
                            <div class="notLogin">
                                <h3>Anda belum melakukan login.</h3>
                                <p class=" mb-2">Silahkan Login/Daftar Akun, <a href="login">Klik Disini <i class="fas fa-sign-in-alt"></i></a></p>
                            </div>
                        @else
                            <ul style="padding-left: 15px;">
                                <li>Nama : <span class="d-block fw-bold">{{ Auth::guard('member')->user()->fullname }}</span></li>
                                <li>Tanggal Lahir : <span class="d-block fw-bold">{{ \Carbon\Carbon::parse(Auth::guard('member')->user()->date_birth)->format('j F Y') }}</span></li>
                                <li>No HP : <span class="d-block fw-bold">{{ Auth::guard('member')->user()->number_phone }}</span></li>
                            </ul>
                        @endif
                        <hr>
                        <h1 class="title">Ringkasan Belanja</h1>
                        <div class="shoppingDetails">
                            <div class="text">Total Harga (<span class="countProduct">0</span> Produk)</div>
                            <div class="total" id="totalPrice">Rp0</div>
                            <input type="hidden" name="totalPrice" value="0">
                        </div>
                        <hr>
                        <h1 class="title">Catatan</h1>
                        <div class="mb-2">
                            <textarea name="catatan" class="form-control form-control-sm" rows="3" placeholder="Isi catatan untuk pesanan anda (Jika Ada)"></textarea>
                        </div>
                        <hr>
                        <h1 class="finalPrice"><span>Total Bayar</span><span id="finalPrice">0</span></h1>
                        <button class="btn btn-submit mt-3" id="orderNow">Jual Sekarang</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('importfootAppend')
    <script src="js/arsdash/select2.min.js"></script>
    <script src="js/arsdash/jquery.validate.min.js"></script>
    <script src="js/arsdash/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".select2").select2({
                allowClear: false,
                width: '100%',
                placeholder: 'Harap Pilih Salah Satu'
            });
        });

        function addCommas(nStr) {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
        let products = [];
        let totalpay = 0;
        let totalprod = 0;
        let finalPrice = 0;
        if (localStorage.getItem('cart')) {
            products = JSON.parse(localStorage.getItem('cart'));
        };
        if (products.length == 0) {
            $('.cartList').html(`<li class="emptyCart"><h1><i class="fa-solid fa-cart-plus"></i> Keranjang Kosong!</h1><a href="katalog" class="btn btn-sm">Tukarkan Sampah Anda</a></li>`);
        }
        for (let i = 0; i < products.length; i++) {
            var total = products[i].pay;
            totalpay = totalpay + total;
            totalprod = totalprod + 1;
            $('.cartList').append(`
                <li>
                    <div class="w-100 d-flex mb-2">
                        <a href="` + products[i].url + `" class="productImage">
                            <img src="` + products[i].img + `" alt="">
                        </a>
                        <div class="productInfo">
                            <a href="` + products[i].url + `"><h1 class="title">` + products[i].title + `</h1></a>
                            <div class="price">Rp` + addCommas(products[i].price) + `/Kg</div>
                            <div class="act d-flex align-items-center">
                                <div data-id="` + products[i].id + `" class="removeList me-2 text-danger">
                                    <i class="fa-regular fa-trash-can"></i> Hapus
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 d-flex justify-content-between align-items-center subTotalInfo">
                        <span class="textSubTotal">Subtotal</span>
                        <span class="subTotal" data-price="` + total + `">Rp` + addCommas(total) + `/<span class="fw-normal">` + products[i].qty + `Kg</span></span>
                    </div>
                </li>
                `);
        };
        $('#totalPrice').html('Rp' + addCommas(totalpay));
        $('input:hidden[name="totalPrice"]').val(totalpay);
        $('#finalPrice').html('Rp' + addCommas(totalpay));
        $('.countProduct').html(totalprod);
        $('.removeList').click(function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            if (products.length > 1) {
                for (let i = 0; i < products.length; i++) {
                    if (products[i].id == id) {
                        totalpay = totalpay - products[i].pay;
                        totalprod = totalprod - 1;
                        products.splice(i, 1);
                    }
                }
            } else {
                totalpay = totalpay - products[0].pay;
                totalprod = totalprod - 1;
                products = [];
                $('.cartList').html(`<li class="emptyCart"><h1><i class="fa-solid fa-cart-plus"></i> Keranjang Kosong!</h1><a href="katalog" class="btn btn-sm">Tukarkan Sampah Anda</a></li>`);
            }
            $('#totalPrice').html('Rp' + addCommas(totalpay));
            $('input:hidden[name="totalPrice"]').val(totalpay);
            $('#finalPrice').html('Rp' + addCommas(totalpay));
            $('.countProduct').html(totalprod);
            $('.cartCount').html(products.length);
            localStorage.setItem('cart', JSON.stringify(products));
            $(this).parents('li').remove();
        });
        $('#orderNow').click(function(e) {
            e.preventDefault();
            @if (Auth::guard('member')->user())
                if (products.length > 0) {
                    var member_id = $('#member_id').val();
                    $.ajax({
                        type: 'POST',
                        url: 'order',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            product: JSON.stringify(products),
                            jumlah_produk: totalprod,
                            total_harga: totalpay,
                            catatan: $('textarea[name="catatan"]').val()
                        },
                        success: function(response) {
                            if (!response.error) {
                                toastr.success(response.message, 'Success !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                setTimeout(function() {
                                    localStorage.removeItem('cart');
                                    location.href = '/';
                                }, 1500);
                            } else {
                                toastr.error(response.error.message, 'Gagal !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                                if (products.length > 1) {
                                    for (let i = 0; i < products.length; i++) {
                                        if (products[i].id == response.error.id) {
                                            totalpay = totalpay - products[i].pay;
                                            totalprod = totalprod - 1;
                                            products.splice(i, 1);
                                        }
                                    }
                                } else {
                                    totalpay = totalpay - products[0].pay;
                                    totalprod = totalprod - 1;
                                    products = [];
                                    $('.cartList').html(`<li class="emptyCart"><h1><i class="fa-solid fa-cart-plus"></i> Keranjang Kosong!</h1><a href="katalog" class="btn btn-sm">Tukarkan Sampah Anda</a></li>`);
                                }
                                $('#totalPrice').html('Rp' + addCommas(totalpay));
                                $('input:hidden[name="totalPrice"]').val(totalpay);
                                $('#finalPrice').html('Rp' + addCommas(totalpay));
                                $('.countProduct').html(totalprod);
                                $('.cartCount').html(products.length);
                                localStorage.setItem('cart', JSON.stringify(products));
                            }
                        },
                        error: function(response) {
                            btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                            $.each(response.error, function(i, field) {
                                toastr.error(i + ' ' + field, 'Gagal !', {
                                    closeButton: true,
                                    progressBar: true,
                                    timeOut: 1500
                                });
                            });
                        }
                    });
                } else {
                    toastr.error('Keranjang anda kosong', 'Gagal !', {
                        closeButton: true,
                        progressBar: true,
                        timeOut: 1500
                    });
                }
            @else
                toastr.error('Anda belum login', 'Gagal !', {
                    closeButton: true,
                    progressBar: true,
                    timeOut: 1500
                });
            @endif
        });
    </script>
@endsection
