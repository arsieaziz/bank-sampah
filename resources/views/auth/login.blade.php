<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ $title }}</title>
    <base href="{{ url('') }}" />
    <meta name="title" content="{{ $title }}" />
    <meta name="description" content="{{ $description }}" />
    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#3a6073">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#3a6073">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="css/arsdash/vendor.css">
    <link rel="stylesheet" href="css/arsdash/main.css">
    <link rel="stylesheet" href="css/arsdash/toastr.min.css">
    <script src="js/arsdash/modernizr.js"></script>
</head>

<body>
    <div class="preloader-wrapper">
        <div class="preloader">
            <img src="images/logo.png" alt="Logo">
        </div>
    </div>
    <main>
        <section class="body-login">
            <div class="container">
                <div class="d-flex justify-content-center align-items-center form-place">
                    <div class="row justify-content-center align-items-center w-100">
                        <div class="col-lg-4 col-md-6 form-box">
                            <div class="form-content">
                                <div class="logo">
                                    <img src="images/logo.png" class="img-fluid bg-img">
                                    <h1 class="text-white">{{ $sitename }}</h1>
                                </div>
                                <div class="content">
                                    <h1>Sign in to your account</h1>
                                    <form action="/auth" id="form-login" autocomplete="off" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-sm" name="username" required="required" minlength="1" placeholder="Username" loginRegex>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="password" class="form-control form-control-sm" name="password" required="required" minlength="1" placeholder="Password" />
                                                <div class="input-group-append">
                                                    <button class="btn btn-sm btn-outline-secondary btn-show-password" type="button"><i class="far fa-eye"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-sm btn-login"><i class="fas fa-sign-in-alt"></i> Login</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <script src="js/arsdash/jquery.js"></script>
        <script src="js/arsdash/main.js"></script>
        <script src="js/arsdash/jquery.validate.min.js"></script>
        <script src="js/arsdash/toastr.min.js"></script>

        <script>
            $(".btn-show-password").click(function(e) {
                e.preventDefault();
                var target = $(this).parents(".input-group").find("input");
                if (target.attr("type") == "password") {
                    target.attr("type", "text");
                } else {
                    target.attr("type", "password");
                }
            });
            $(document).ready(function() {
                $("#form-login").validate({
                    highlight: function(element) {
                        $(element).closest('.form-group').find('.invalid').removeClass('d-none').addClass(
                            'd-block');
                    },
                    unhighlight: function(element) {
                        $(element).closest('.form-group').find('.invalid').removeClass('d-block').addClass(
                            'd-none');
                    },
                    errorElement: 'label',
                    errorClass: 'd-block invalid',
                    errorPlacement: function(error, element) {
                        error.appendTo(element.closest('.form-group'));
                    },
                    submitHandler: function(form, eve) {
                        eve.preventDefault();
                        var myform = $(form);
                        var btnSubmit = myform.find("[type='submit']");
                        var btnSubmitHtml = btnSubmit.html();
                        var url = myform.attr("action");
                        var data = new FormData(form);
                        $.ajax({
                            beforeSend: function() {
                                btnSubmit.addClass("disabled").html(
                                    "<i class='fa fa-spinner fa-pulse fa-fw'></i> Loading ... "
                                );
                            },
                            cache: false,
                            processData: false,
                            contentType: false,
                            type: "POST",
                            url: url,
                            data: data,
                            dataType: 'JSON',
                            success: function(response) {
                                if (response.status == 'success') {
                                    btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                    toastr.success(response.message, 'Success !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 1000
                                    });
                                    setTimeout(function() {
                                        if (response.redirect == "" || response
                                            .redirect == "reload") {
                                            location.reload();
                                        } else {
                                            location.href = response.redirect;
                                        }
                                    }, 1000);
                                } else {
                                    btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                    toastr.error(response.message, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 3000
                                    });
                                }
                            },
                            error: function(response) {
                                btnSubmit.removeClass("disabled").html(btnSubmitHtml);
                                $.each(response.error, function(i, field) {
                                    toastr.error(i + ' ' + field, 'Failed !', {
                                        closeButton: true,
                                        progressBar: true,
                                        timeOut: 3000
                                    });
                                });
                            }
                        });
                    }
                });
            });
        </script>
    </main>
</body>

</html>
